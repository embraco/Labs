﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabCommon;
using MySDK;

namespace Lab_11_1
{
    /// <summary>
    /// 1. Створити дерево. Додати до вузла I вузол К з використанням класу
    /// List<T>. Вивести елементи дерева на консоль з вказівкою рівнів.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(45, 12);
            _terminal.Title = "Лабораторная работа №11.1";
            //аж голова закружилась пока состовлял дерево!!!
            CTree<string> _tree = 
                new CTree<string>("F",
                    new CTree<string>("B",
                        new CTree<string>("A"),
                        new CTree<string>("D",
                            new CTree<string>("C"),
                            new CTree<string>("E"))),
                    new CTree<string>("G",
                        new CTree<string>("I",
                            new CTree<string>("H"),
                            new CTree<string>("K")))
                    );

            List<string> _string_tree = _tree.GetStringTree();
            for(int i = 0; i < _string_tree.Count; i++)
            {
                _terminal.PrintAt(0, i, _string_tree[i]);
            }

            Console.ReadKey();
            _terminal.Close();
        }
    }
}
