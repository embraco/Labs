﻿
namespace Lab_13_all
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.fm_l_name = new System.Windows.Forms.Label();
            this.fm_l_email = new System.Windows.Forms.Label();
            this.fm_l_phone = new System.Windows.Forms.Label();
            this.fm_tb_name = new System.Windows.Forms.TextBox();
            this.fm_tb_email = new System.Windows.Forms.TextBox();
            this.fm_tb_phone = new System.Windows.Forms.TextBox();
            this.fm_b_check_1 = new System.Windows.Forms.Button();
            this.fm_l_error = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.fm_tss_owner = new System.Windows.Forms.ToolStripStatusLabel();
            this.fm_tb_number = new System.Windows.Forms.TextBox();
            this.fm_gb_task_1 = new System.Windows.Forms.GroupBox();
            this.fm_gb_task_2 = new System.Windows.Forms.GroupBox();
            this.fm_l_error_2 = new System.Windows.Forms.Label();
            this.fm_b_check_2 = new System.Windows.Forms.Button();
            this.fm_tb_text = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fm_tb_find = new System.Windows.Forms.TextBox();
            this.fm_l_error_3 = new System.Windows.Forms.Label();
            this.fm_b_find_1 = new System.Windows.Forms.Button();
            this.fm_tb_change_where = new System.Windows.Forms.TextBox();
            this.fm_tb_change_hawe = new System.Windows.Forms.TextBox();
            this.fm_b_exchenge = new System.Windows.Forms.Button();
            this.fm_l_change_where = new System.Windows.Forms.Label();
            this.fm_l_change_hawe = new System.Windows.Forms.Label();
            this.fm_b_clear = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fm_b_parse = new System.Windows.Forms.Button();
            this.fm_tb_html = new System.Windows.Forms.TextBox();
            this.fm_b_clear_html = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.fm_gb_task_1.SuspendLayout();
            this.fm_gb_task_2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // fm_l_name
            // 
            this.fm_l_name.AutoSize = true;
            this.fm_l_name.Location = new System.Drawing.Point(150, 22);
            this.fm_l_name.Name = "fm_l_name";
            this.fm_l_name.Size = new System.Drawing.Size(26, 13);
            this.fm_l_name.TabIndex = 0;
            this.fm_l_name.Text = "Ім\'я";
            // 
            // fm_l_email
            // 
            this.fm_l_email.AutoSize = true;
            this.fm_l_email.Location = new System.Drawing.Point(150, 49);
            this.fm_l_email.Name = "fm_l_email";
            this.fm_l_email.Size = new System.Drawing.Size(105, 13);
            this.fm_l_email.TabIndex = 1;
            this.fm_l_email.Text = "електронна адреса";
            // 
            // fm_l_phone
            // 
            this.fm_l_phone.AutoSize = true;
            this.fm_l_phone.Location = new System.Drawing.Point(150, 76);
            this.fm_l_phone.Name = "fm_l_phone";
            this.fm_l_phone.Size = new System.Drawing.Size(92, 13);
            this.fm_l_phone.TabIndex = 2;
            this.fm_l_phone.Text = "Номер телефону";
            // 
            // fm_tb_name
            // 
            this.fm_tb_name.Location = new System.Drawing.Point(6, 19);
            this.fm_tb_name.Name = "fm_tb_name";
            this.fm_tb_name.Size = new System.Drawing.Size(138, 20);
            this.fm_tb_name.TabIndex = 3;
            // 
            // fm_tb_email
            // 
            this.fm_tb_email.Location = new System.Drawing.Point(6, 46);
            this.fm_tb_email.Name = "fm_tb_email";
            this.fm_tb_email.Size = new System.Drawing.Size(138, 20);
            this.fm_tb_email.TabIndex = 4;
            // 
            // fm_tb_phone
            // 
            this.fm_tb_phone.Location = new System.Drawing.Point(6, 73);
            this.fm_tb_phone.Name = "fm_tb_phone";
            this.fm_tb_phone.Size = new System.Drawing.Size(138, 20);
            this.fm_tb_phone.TabIndex = 5;
            // 
            // fm_b_check_1
            // 
            this.fm_b_check_1.Location = new System.Drawing.Point(6, 100);
            this.fm_b_check_1.Name = "fm_b_check_1";
            this.fm_b_check_1.Size = new System.Drawing.Size(75, 23);
            this.fm_b_check_1.TabIndex = 6;
            this.fm_b_check_1.Text = "Перевірити";
            this.fm_b_check_1.UseVisualStyleBackColor = true;
            // 
            // fm_l_error
            // 
            this.fm_l_error.AutoSize = true;
            this.fm_l_error.Location = new System.Drawing.Point(87, 105);
            this.fm_l_error.Name = "fm_l_error";
            this.fm_l_error.Size = new System.Drawing.Size(28, 13);
            this.fm_l_error.TabIndex = 7;
            this.fm_l_error.Text = "error";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fm_tss_owner});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // fm_tss_owner
            // 
            this.fm_tss_owner.Name = "fm_tss_owner";
            this.fm_tss_owner.Size = new System.Drawing.Size(28, 17);
            this.fm_tss_owner.Text = "Ім\'я";
            // 
            // fm_tb_number
            // 
            this.fm_tb_number.Location = new System.Drawing.Point(6, 19);
            this.fm_tb_number.Name = "fm_tb_number";
            this.fm_tb_number.Size = new System.Drawing.Size(158, 20);
            this.fm_tb_number.TabIndex = 9;
            // 
            // fm_gb_task_1
            // 
            this.fm_gb_task_1.Controls.Add(this.fm_tb_name);
            this.fm_gb_task_1.Controls.Add(this.fm_l_name);
            this.fm_gb_task_1.Controls.Add(this.fm_l_email);
            this.fm_gb_task_1.Controls.Add(this.fm_l_error);
            this.fm_gb_task_1.Controls.Add(this.fm_l_phone);
            this.fm_gb_task_1.Controls.Add(this.fm_b_check_1);
            this.fm_gb_task_1.Controls.Add(this.fm_tb_email);
            this.fm_gb_task_1.Controls.Add(this.fm_tb_phone);
            this.fm_gb_task_1.Location = new System.Drawing.Point(12, 12);
            this.fm_gb_task_1.Name = "fm_gb_task_1";
            this.fm_gb_task_1.Size = new System.Drawing.Size(256, 128);
            this.fm_gb_task_1.TabIndex = 10;
            this.fm_gb_task_1.TabStop = false;
            this.fm_gb_task_1.Text = "Завдання №1";
            // 
            // fm_gb_task_2
            // 
            this.fm_gb_task_2.Controls.Add(this.fm_l_error_2);
            this.fm_gb_task_2.Controls.Add(this.fm_b_check_2);
            this.fm_gb_task_2.Controls.Add(this.fm_tb_number);
            this.fm_gb_task_2.Location = new System.Drawing.Point(274, 12);
            this.fm_gb_task_2.Name = "fm_gb_task_2";
            this.fm_gb_task_2.Size = new System.Drawing.Size(178, 128);
            this.fm_gb_task_2.TabIndex = 11;
            this.fm_gb_task_2.TabStop = false;
            this.fm_gb_task_2.Text = "Завдання №2";
            // 
            // fm_l_error_2
            // 
            this.fm_l_error_2.AutoSize = true;
            this.fm_l_error_2.Location = new System.Drawing.Point(88, 54);
            this.fm_l_error_2.Name = "fm_l_error_2";
            this.fm_l_error_2.Size = new System.Drawing.Size(28, 13);
            this.fm_l_error_2.TabIndex = 11;
            this.fm_l_error_2.Text = "error";
            // 
            // fm_b_check_2
            // 
            this.fm_b_check_2.Location = new System.Drawing.Point(7, 49);
            this.fm_b_check_2.Name = "fm_b_check_2";
            this.fm_b_check_2.Size = new System.Drawing.Size(75, 23);
            this.fm_b_check_2.TabIndex = 10;
            this.fm_b_check_2.Text = "Перевірити";
            this.fm_b_check_2.UseVisualStyleBackColor = true;
            // 
            // fm_tb_text
            // 
            this.fm_tb_text.Location = new System.Drawing.Point(6, 19);
            this.fm_tb_text.Multiline = true;
            this.fm_tb_text.Name = "fm_tb_text";
            this.fm_tb_text.ReadOnly = true;
            this.fm_tb_text.Size = new System.Drawing.Size(255, 253);
            this.fm_tb_text.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fm_b_clear);
            this.groupBox1.Controls.Add(this.fm_l_change_hawe);
            this.groupBox1.Controls.Add(this.fm_l_change_where);
            this.groupBox1.Controls.Add(this.fm_b_exchenge);
            this.groupBox1.Controls.Add(this.fm_tb_change_hawe);
            this.groupBox1.Controls.Add(this.fm_tb_change_where);
            this.groupBox1.Controls.Add(this.fm_tb_find);
            this.groupBox1.Controls.Add(this.fm_l_error_3);
            this.groupBox1.Controls.Add(this.fm_b_find_1);
            this.groupBox1.Controls.Add(this.fm_tb_text);
            this.groupBox1.Location = new System.Drawing.Point(12, 146);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 278);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Завдання №3-4";
            // 
            // fm_tb_find
            // 
            this.fm_tb_find.Location = new System.Drawing.Point(267, 19);
            this.fm_tb_find.Name = "fm_tb_find";
            this.fm_tb_find.Size = new System.Drawing.Size(100, 20);
            this.fm_tb_find.TabIndex = 16;
            // 
            // fm_l_error_3
            // 
            this.fm_l_error_3.AutoSize = true;
            this.fm_l_error_3.Location = new System.Drawing.Point(268, 42);
            this.fm_l_error_3.Name = "fm_l_error_3";
            this.fm_l_error_3.Size = new System.Drawing.Size(28, 13);
            this.fm_l_error_3.TabIndex = 15;
            this.fm_l_error_3.Text = "error";
            // 
            // fm_b_find_1
            // 
            this.fm_b_find_1.Location = new System.Drawing.Point(267, 58);
            this.fm_b_find_1.Name = "fm_b_find_1";
            this.fm_b_find_1.Size = new System.Drawing.Size(75, 23);
            this.fm_b_find_1.TabIndex = 13;
            this.fm_b_find_1.Text = "Знайти";
            this.fm_b_find_1.UseVisualStyleBackColor = true;
            // 
            // fm_tb_change_where
            // 
            this.fm_tb_change_where.Location = new System.Drawing.Point(267, 87);
            this.fm_tb_change_where.Name = "fm_tb_change_where";
            this.fm_tb_change_where.Size = new System.Drawing.Size(100, 20);
            this.fm_tb_change_where.TabIndex = 17;
            // 
            // fm_tb_change_hawe
            // 
            this.fm_tb_change_hawe.Location = new System.Drawing.Point(267, 113);
            this.fm_tb_change_hawe.Name = "fm_tb_change_hawe";
            this.fm_tb_change_hawe.Size = new System.Drawing.Size(100, 20);
            this.fm_tb_change_hawe.TabIndex = 18;
            // 
            // fm_b_exchenge
            // 
            this.fm_b_exchenge.Location = new System.Drawing.Point(267, 140);
            this.fm_b_exchenge.Name = "fm_b_exchenge";
            this.fm_b_exchenge.Size = new System.Drawing.Size(75, 23);
            this.fm_b_exchenge.TabIndex = 19;
            this.fm_b_exchenge.Text = "Замінити";
            this.fm_b_exchenge.UseVisualStyleBackColor = true;
            // 
            // fm_l_change_where
            // 
            this.fm_l_change_where.AutoSize = true;
            this.fm_l_change_where.Location = new System.Drawing.Point(374, 93);
            this.fm_l_change_where.Name = "fm_l_change_where";
            this.fm_l_change_where.Size = new System.Drawing.Size(53, 13);
            this.fm_l_change_where.TabIndex = 20;
            this.fm_l_change_where.Text = "Замінити";
            // 
            // fm_l_change_hawe
            // 
            this.fm_l_change_hawe.AutoSize = true;
            this.fm_l_change_hawe.Location = new System.Drawing.Point(374, 119);
            this.fm_l_change_hawe.Name = "fm_l_change_hawe";
            this.fm_l_change_hawe.Size = new System.Drawing.Size(39, 13);
            this.fm_l_change_hawe.TabIndex = 21;
            this.fm_l_change_hawe.Text = "На що";
            // 
            // fm_b_clear
            // 
            this.fm_b_clear.Location = new System.Drawing.Point(268, 170);
            this.fm_b_clear.Name = "fm_b_clear";
            this.fm_b_clear.Size = new System.Drawing.Size(99, 23);
            this.fm_b_clear.TabIndex = 22;
            this.fm_b_clear.Text = "Відновити текст";
            this.fm_b_clear.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fm_b_clear_html);
            this.groupBox2.Controls.Add(this.fm_tb_html);
            this.groupBox2.Controls.Add(this.fm_b_parse);
            this.groupBox2.Location = new System.Drawing.Point(459, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(329, 411);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Завдання №5";
            // 
            // fm_b_parse
            // 
            this.fm_b_parse.Location = new System.Drawing.Point(7, 21);
            this.fm_b_parse.Name = "fm_b_parse";
            this.fm_b_parse.Size = new System.Drawing.Size(92, 23);
            this.fm_b_parse.TabIndex = 0;
            this.fm_b_parse.Text = "Виділити текст";
            this.fm_b_parse.UseVisualStyleBackColor = true;
            // 
            // fm_tb_html
            // 
            this.fm_tb_html.Location = new System.Drawing.Point(7, 50);
            this.fm_tb_html.Multiline = true;
            this.fm_tb_html.Name = "fm_tb_html";
            this.fm_tb_html.ReadOnly = true;
            this.fm_tb_html.Size = new System.Drawing.Size(316, 276);
            this.fm_tb_html.TabIndex = 1;
            // 
            // fm_b_clear_html
            // 
            this.fm_b_clear_html.Location = new System.Drawing.Point(106, 21);
            this.fm_b_clear_html.Name = "fm_b_clear_html";
            this.fm_b_clear_html.Size = new System.Drawing.Size(75, 23);
            this.fm_b_clear_html.TabIndex = 2;
            this.fm_b_clear_html.Text = "Відновити";
            this.fm_b_clear_html.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.fm_gb_task_2);
            this.Controls.Add(this.fm_gb_task_1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.fm_gb_task_1.ResumeLayout(false);
            this.fm_gb_task_1.PerformLayout();
            this.fm_gb_task_2.ResumeLayout(false);
            this.fm_gb_task_2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label fm_l_name;
        private System.Windows.Forms.Label fm_l_email;
        private System.Windows.Forms.Label fm_l_phone;
        private System.Windows.Forms.TextBox fm_tb_name;
        private System.Windows.Forms.TextBox fm_tb_email;
        private System.Windows.Forms.TextBox fm_tb_phone;
        private System.Windows.Forms.Button fm_b_check_1;
        private System.Windows.Forms.Label fm_l_error;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel fm_tss_owner;
        private System.Windows.Forms.TextBox fm_tb_number;
        private System.Windows.Forms.GroupBox fm_gb_task_1;
        private System.Windows.Forms.GroupBox fm_gb_task_2;
        private System.Windows.Forms.Button fm_b_check_2;
        private System.Windows.Forms.Label fm_l_error_2;
        private System.Windows.Forms.TextBox fm_tb_text;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button fm_b_find_1;
        private System.Windows.Forms.Label fm_l_error_3;
        private System.Windows.Forms.TextBox fm_tb_find;
        private System.Windows.Forms.Label fm_l_change_hawe;
        private System.Windows.Forms.Label fm_l_change_where;
        private System.Windows.Forms.Button fm_b_exchenge;
        private System.Windows.Forms.TextBox fm_tb_change_hawe;
        private System.Windows.Forms.TextBox fm_tb_change_where;
        private System.Windows.Forms.Button fm_b_clear;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox fm_tb_html;
        private System.Windows.Forms.Button fm_b_parse;
        private System.Windows.Forms.Button fm_b_clear_html;
    }
}

