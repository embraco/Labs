﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_12_1
{
    internal class CBinaryTree<T>
    {
        internal T Value { get; set; }
        internal CBinaryTree<T> LeftChild { get; set; }
        internal CBinaryTree<T> RightChild { get; set; }

        public CBinaryTree(T value, CBinaryTree<T> left_child, CBinaryTree<T> right_child)
        {
            Value = value;
            LeftChild = left_child;
            RightChild = right_child;
        }

        public CBinaryTree(T value) : this(value, null, null) { }
        public void GetStringList()
        {
            if (LeftChild != null)
                LeftChild.GetStringList();
            Console.Write($"{Value} ");
            if (RightChild != null)
                RightChild.GetStringList();
        }
    }
}
