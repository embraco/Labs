﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_11_6
{
    /// <summary>
    /// 5. TreeView. Створити проект Windows Form. Створити в конструкторі
    /// дерево структури МНТУ.Додати до вузла факультету Економіки 2 вузли:
    /// Кафедра Міжнародної економіки та Фінанси.До цих кафедр додати по
    /// одній групі: МЕ-81 та ФН-71. Реалізувати функції обходу дерева,
    /// додавання та видалення вузлів.При виборі груп вивести в ListBox
    /// прізвища студентів.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
