﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySDK;

namespace Lab_11_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            fm_tv_list.AfterSelect += EventAfterSelect;
            fm_b_add.Click += EventButtonAdd;
            fm_b_delete.Click += EventButtonDel;
            fm_b_exit.Click += (s, e) => { Close(); };
            this.Load += EventLoad;
            fm_b_list.Click += EventButtonList;
        }

        private void EventButtonList(object s, EventArgs e)
        {
            fm_lb_list.Items.Clear();
            GetStringListDir(fm_tv_list.Nodes[0], 0);
        }
        private void GetStringListDir(TreeNode _nod, int _depth)
        {
            fm_lb_list.Items.Add($"{new string(' ', _depth * 2)}{_depth}:{_nod.Text}");
            TreeNodeCollection _child = _nod.Nodes;
            foreach (TreeNode _n in _child)
                GetStringListDir(_n, _depth + 1);
        }
        private void EventLoad(object s, EventArgs e)
        {
            CRM _rm = new CRM();
            fm_tlss_ovner.Text = _rm.GetRecource("owner");
            this.Text = "Лабораторная работа №11.6";
            TreeNode _node = new TreeNode("Лазерні");
            _node.Nodes.AddRange(new TreeNode[] { new TreeNode { Name = "деякий опис лазерна модель 1", Text = "лазерна модель 1" },
                                                  new TreeNode {Name = "деякий опис лазерна модель 2", Text = "лазерна модель 2" } });
            TreeNode _node_2 = new TreeNode("Струменеві");
            _node_2.Nodes.AddRange(new TreeNode[] { new TreeNode { Name = "деякий опис струменева модель 1", Text = "струменева модель 1" },
                                                    new TreeNode { Name = "деякий опис струменева модель 2", Text = "струменева модель 2" } });
            TreeNode _node_3 = new TreeNode("ПК");
            _node_3.Nodes.AddRange(new TreeNode[] { new TreeNode { Name = "деякий опис ПК модель 1", Text = "ПК модель 1" },
                                                    new TreeNode { Name = "деякий опис ПК модель 2", Text = "ПК модель 2" } });
            TreeNode _node_4 = new TreeNode("Ноутбуки");
            _node_4.Nodes.AddRange(new TreeNode[] { new TreeNode { Name = "деякий опис ноутбук модель 1", Text = "ноутбук модель 1" },
                                                    new TreeNode { Name = "деякий опис ноутбук модель 2", Text = "ноутбук модель 2" } });
            TreeNode _node_5 = new TreeNode("Комп'ютерна техніка");
            _node_5.Nodes.AddRange(new TreeNode[] { _node_3, _node_4});
            TreeNode _node_6 = new TreeNode("Птинтери");
            _node_6.Nodes.AddRange(new TreeNode[] { _node, _node_2 });
            TreeNode _node_7 = new TreeNode("Товари");
            _node_7.Nodes.AddRange(new TreeNode[] { _node_5, _node_6 });

            fm_tv_list.Nodes.Add(_node_7);
        }

        private void EventButtonDel(object s, EventArgs e)
        {
            if(fm_tv_list.SelectedNode != null)
                fm_tv_list.SelectedNode.Remove();
        }

        private void EventButtonAdd(object s, EventArgs e)
        {
            TreeNode _node = new TreeNode();
            _node.Text = fm_tb_name.Text;
            _node.Name = fm_tb_text.Text;
            if (fm_tv_list.SelectedNode == null)
                fm_tv_list.Nodes.Add(_node);
            else
                fm_tv_list.SelectedNode.Nodes.Add(_node);
        }

        private void EventAfterSelect(object s, TreeViewEventArgs e)
        {
            fm_lb_list.Items.Clear();
            fm_lb_list.Items.Add(e.Node.Name);
        }
    }
}