﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_20_2
{
    /// <summary>
    /// 2. Реалізувати Windows-проект для обчислення добутку двох
    /// десяткових чисел.Вхідні числа вибираються в елементі
    /// NumericUpDown.Результат вивести в текстове поле.
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Load += LoadForm;
            nUD_a.ValueChanged += Calculate;
            nUD_b.ValueChanged += Calculate;
        }

        //мой код
        private void LoadForm(object s, EventArgs e)
        {
            ResourceManager rm = new ResourceManager("Lab_20_2.db", Assembly.GetExecutingAssembly());
            char[] msg = rm.GetString("name").ToCharArray();
            for (int i = 0; i < msg.Length; i++) msg[i] = (char)(msg[i] - 2);
            this.toolStripStatusLabel2.Text = new string(msg);
            this.Text += " №20.2";
        }

        private void CloseForm(object s, EventArgs e)
        {
            Close();
        }

        private void Calculate(object s, EventArgs e)
        {
            tB_res.Text =  $"{nUD_a.Value * nUD_b.Value}";
        }
    }
}
