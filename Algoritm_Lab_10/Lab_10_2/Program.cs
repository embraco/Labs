﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySDK;

namespace Lab_10_2
{
    class Program
    {
        /// <summary>
        /// 2. Клас HashSet<T>. 
        /// Створити множину з масиву {0,2,4,7,2,1,10,0,0}.
        /// Вивести елементи множини на консоль.Видалити з неї всі 2 та 0.
        /// Результат вивести на консоль.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(120, 24);
            _terminal.Title = "Лабораторна робота №10.2";

            //возможно данное задание необходимо было выполнить с List<>
            //т.к. HashSet<> удаляет одинаковые элементы
            int[] _mass = { 0, 2, 4, 7, 2, 1, 10, 0, 0 };
            HashSet<int> _mass_one = new HashSet<int>(_mass);

            _terminal.PrintAt(0, 0, "Масив");
            _terminal.PrintAt(0, 1, GetArrString<int>(_mass));
            _terminal.PrintAt(0, 2, "Множина");
            _terminal.PrintAt(0, 3, GetArrString<int>(_mass_one));
            _mass_one.Remove(0);
            _mass_one.Remove(2);
            _terminal.PrintAt(0, 4, "Множина з видаленими 0 та 2");
            _terminal.PrintAt(0, 5, GetArrString<int>(_mass_one));

            Console.ReadKey();
            _terminal.Close();
        }

        private static string GetArrString<T>(HashSet<T> _value)
        {
            string _msg = "{";
            bool _first = true;
            foreach (T _elem in _value)
            {
                if (_first)
                {
                    _first = !_first;
                    _msg += $"{_elem}";
                }
                else
                {
                    _msg += $",{_elem}";
                }
            }
            _msg += "}";
            return _msg;
        }
        private static string GetArrString<T>(T[] _value)
        {
            string _msg = "{";
            bool _first = true;
            foreach (T _elem in _value)
            {
                if (_first)
                {
                    _first = !_first;
                    _msg += $"{_elem}";
                }
                else
                {
                    _msg += $",{_elem}";
                }
            }
            _msg += "}";
            return _msg;
        }
    }
}
