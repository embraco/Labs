﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_12_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] elem = new int[] { 1, 3, 6, 8, 10, 14};
            CBinTreeInt tree = new CBinTreeInt();
            foreach (int _i in elem)
                tree.Insert(_i);
            tree.Insert(5);
            tree.Insert(15);
            Console.WriteLine("содержит ли дерево 14");
            Console.WriteLine(tree.Contains(14) ? "содержит" : "не содержит");
            tree.PritnTreeDFS();
            Console.WriteLine("удалить значение 1");
            tree.Remove(1);
            tree.PritnTreeDFS();
            Console.WriteLine("добавился ли 5");
            Console.WriteLine(tree.Contains(5) ? "добавился" : "не добавился");
            Console.WriteLine("минимальное значение");
            Console.WriteLine(tree.Min());
            Console.WriteLine("количество элементов");
            Console.WriteLine(tree.Count());

            //задание 3
            Console.WriteLine("сумма елементов");
            Console.WriteLine(tree.GetSum());
            Console.ReadKey();
        }
    }
}
