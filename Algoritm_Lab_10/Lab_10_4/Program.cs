﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySDK;

namespace Lab_10_4
{
    /// <summary>
    /// 4. Клас SortedSet<T>Створити дві множини класу
    /// SortedSet<string>.Перша містить назви товарів в магазині
    /// { хліб, молоко, ковбаса }. 
    /// Друга містить більшу номенклатуру, 
    /// 10 товарів серед яких є товари з магазину.
    /// Визначити, яких товарів не вистачає  в магазині.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(120, 24);
            _terminal.Title = "Лабораторна робота №10.4";

            SortedSet<string> _mass_one = new SortedSet<string>() { "хлiб", "молоко", "ковбаса" };
            SortedSet<string> _mass_thwo = new SortedSet<string>() { "капуста", "риба", "м'ясо", "печево", "морозиво", "картопля", "чiпси", "хлiб", "молоко", "ковбаса" };

            _terminal.PrintAt(0, 0, "Товари у магазинi");
            _terminal.PrintAt(0, 1, GetHashString<string>(_mass_one));
            _terminal.PrintAt(0, 2, "Товари на складi");
            _terminal.PrintAt(0, 3, GetHashString<string>(_mass_thwo));
            _terminal.PrintAt(0, 4, "Товари яких не вистачае у магазинi");
            //следуя теории множеств товары которых не хватает в магазине можно узнать разностью множеств
            SortedSet<string> _mass_tmp = new SortedSet<string>(_mass_thwo);
            _mass_tmp.ExceptWith(_mass_one);
            _terminal.PrintAt(0, 5, GetHashString<string>(_mass_tmp));

            Console.ReadKey();
            _terminal.Close();
        }

        private static string GetHashString<T>(SortedSet<T> _value)
        {
            string _msg = "{";
            bool _first = true;
            foreach (T _elem in _value)
            {
                if (_first)
                {
                    _first = !_first;
                    _msg += $"{_elem}";
                }
                else
                {
                    _msg += $",{_elem}";
                }
            }
            _msg += "}";
            return _msg;
        }
    }
}
