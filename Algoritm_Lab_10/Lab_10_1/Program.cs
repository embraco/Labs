﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySDK;

namespace Lab_10_1
{
    /// <summary>
    /// 1 Клас HashSet<T>
    /// Створити 2 множини цілих чисел, серед яких є однакові елементи, m1={1,4,6,9}, m2 ={ 4,9,0,10,11}
    /// Знайти об’єднання, перетин, різницю та симетричну різницю цих множин.
    /// </summary>
class Program
    {
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(50, 10);
            _terminal.Title = "Лабораторна робота №10.1";

            HashSet<int> _mass_one = new HashSet<int>() { 1, 4, 6, 9 };
            HashSet<int> _mass_thwo = new HashSet<int>() { 4, 9, 0, 10, 11 };

            _terminal.PrintAt(0, 0, "Перша множина");
            _terminal.PrintAt(0, 1, GetHashString<int>(_mass_one));
            _terminal.PrintAt(20, 0, "Друга множина");
            _terminal.PrintAt(20, 1, GetHashString<int>(_mass_thwo));
            _terminal.PrintAt(0, 3, "Об'єднання множин");
            HashSet<int> _mass_tmp = new HashSet<int>(_mass_one);
            _mass_tmp.UnionWith(_mass_thwo);
            _terminal.PrintAt(0, 4, GetHashString<int>(_mass_tmp));
            _terminal.PrintAt(20, 3, "Перетин множин");
            _mass_tmp = new HashSet<int>(_mass_one);
            _mass_tmp.IntersectWith(_mass_thwo);
            _terminal.PrintAt(20, 4, GetHashString<int>(_mass_tmp));
            _terminal.PrintAt(0, 6, "Рiзниця множин");
            _mass_tmp = new HashSet<int>(_mass_one);
            _mass_tmp.ExceptWith(_mass_thwo);
            _terminal.PrintAt(0, 7, GetHashString<int>(_mass_tmp));
            _terminal.PrintAt(20, 6, "Симетрична рiзниця множин");
            _mass_tmp = new HashSet<int>(_mass_one);
            _mass_tmp.SymmetricExceptWith(_mass_thwo);
            _terminal.PrintAt(20, 7, GetHashString<int>(_mass_tmp));

            Console.ReadKey();
            _terminal.Close();
        }

        private static string GetHashString<T>(HashSet<T> _value)
        {
            string _msg = "{";
            bool _first = true;
            foreach(T _elem in _value)
            {
                if (_first)
                {
                    _first = !_first;
                    _msg += $"{_elem}";
                }
                else
                {
                    _msg += $",{_elem}";
                }
            }
            _msg += "}";
            return _msg;
        }
    }
}
