﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab_23_Forms;

namespace Lab_23_2
{
    static class Program
    {
        private static FLab_23_2 m_form;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            m_form = new FLab_23_2();
            m_form.LFolder.Text = "";
            m_form.LFile.Text = "";
            m_form.BOpenFolder.Click += (s, e) =>
            {
                m_form.OpenFolder.ShowDialog();
                m_form.LFolder.Text = m_form.OpenFolder.SelectedPath;
            };
            m_form.BOpenFile.Click += (s, e) =>
            {
                m_form.OpenFile.InitialDirectory = m_form.OpenFolder.SelectedPath;
                m_form.OpenFile.ShowDialog();
                m_form.LFile.Text = m_form.OpenFile.FileName;
            };
            Application.Run(m_form);
        }
    }
}
