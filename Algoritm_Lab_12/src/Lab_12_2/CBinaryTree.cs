﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_12_2
{
    public class CBinaryTree<T> where T : IComparable<T>
    {
        internal CBinaryTreeNode<T> root;

        public CBinaryTree()
        {
            root = null;
        }

        public void Insert(T value)
        {
            root = Insert(value, null, root);
        }

        public bool Contains(T value)
        {
            return Find(value) != null;
        }

        public void PritnTreeDFS()
        {
            PritnTreeDFS(root);
            Console.WriteLine();
        }

        public void Remove(T value)
        {
            CBinaryTreeNode<T> nodeToDelete = Find(value);
            if (nodeToDelete != null)
                Remove(nodeToDelete);
        }

        public T Min()
        {
            return GetMinRecursiv(root);
        }

        public int Count()
        {
            return Count(root);
        }
        private int Count(CBinaryTreeNode<T> node)
        {
            int count = 0;
            if (node == null)
                return count;
            count += Count(node.leftChild);
            count += Count(node.rightChild);
            return count + 1;
        }
        private CBinaryTreeNode<T> Insert(T value, CBinaryTreeNode<T> parentNode, CBinaryTreeNode<T> node)
        {
            if(node == null)
            {
                node = new CBinaryTreeNode<T>(value);
                node.parent = parentNode;
            }
            else
            {
                int compare_to = value.CompareTo(node.value);
                if (compare_to < 0)
                    node.leftChild = Insert(value, node, node.leftChild);
                else if (compare_to > 0)
                    node.rightChild = Insert(value, node, node.rightChild);
            }
            return node;
        }

        private CBinaryTreeNode<T> Find(T value)
        {
            CBinaryTreeNode<T> node = root;
            while(node != null)
            {
                int compare_to = value.CompareTo(node.value);
                if (compare_to < 0)
                    node = node.leftChild;
                else if (compare_to > 0)
                    node = node.rightChild;
                else
                    break;
            }
            return node;
        }

        private void PritnTreeDFS(CBinaryTreeNode<T> node)
        {
            if (node == null)
                return;
            PritnTreeDFS(node.leftChild);
            Console.Write($"{node.value} ");
            PritnTreeDFS(node.rightChild);
        }

        private void Remove(CBinaryTreeNode<T> node)
        {
            if(node.leftChild != null && node.rightChild != null)
            {
                CBinaryTreeNode<T> replacenent = node.rightChild;
                while (replacenent.leftChild != null)
                    replacenent = replacenent.leftChild;
                node.value = replacenent.value;
                node = replacenent;
            }
            CBinaryTreeNode<T> theChild = node.leftChild != null ? node.leftChild : node.rightChild;
            if(theChild != null)
            {
                theChild.parent = node.parent;
                if (node.parent == null)
                    root = theChild;
                else
                {
                    if (node.parent.leftChild == node)
                        node.parent.leftChild = theChild;
                    else
                        node.parent.rightChild = theChild;
                }
            }
            else
            {
                if (node.parent == null)
                    root = null;
                else
                {
                    if (node.parent.leftChild == node)
                        node.parent.leftChild = null;
                    else
                        node.parent.rightChild = null;
                }
            }
        }

        private static T GetMinRecursiv(CBinaryTreeNode<T> node)
        {
            if (node.leftChild == null)
                return node.value;
            else
                return GetMinRecursiv(node.leftChild);
        }
    }
}
