﻿
using System.Reflection;
using System.Resources;

namespace Lab_20_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.f_name = new System.Windows.Forms.Label();
            this.s_name = new System.Windows.Forms.Label();
            this.b_date = new System.Windows.Forms.Label();
            this.group = new System.Windows.Forms.Label();
            this.cours = new System.Windows.Forms.Label();
            this.input_f_name = new System.Windows.Forms.TextBox();
            this.input_s_name = new System.Windows.Forms.TextBox();
            this.input_b_date = new System.Windows.Forms.TextBox();
            this.input_group = new System.Windows.Forms.TextBox();
            this.input_cours = new System.Windows.Forms.TextBox();
            this.text_res = new System.Windows.Forms.TextBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(53, 17);
            this.toolStripStatusLabel1.Text = "Виконав";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(28, 17);
            this.toolStripStatusLabel2.Text = "Ім\'я";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 200);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(523, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // f_name
            // 
            this.f_name.AutoSize = true;
            this.f_name.Location = new System.Drawing.Point(12, 20);
            this.f_name.Name = "f_name";
            this.f_name.Size = new System.Drawing.Size(35, 13);
            this.f_name.TabIndex = 8;
            this.f_name.Text = "label1";
            // 
            // s_name
            // 
            this.s_name.AutoSize = true;
            this.s_name.Location = new System.Drawing.Point(13, 47);
            this.s_name.Name = "s_name";
            this.s_name.Size = new System.Drawing.Size(35, 13);
            this.s_name.TabIndex = 9;
            this.s_name.Text = "label2";
            // 
            // b_date
            // 
            this.b_date.AutoSize = true;
            this.b_date.Location = new System.Drawing.Point(12, 74);
            this.b_date.Name = "b_date";
            this.b_date.Size = new System.Drawing.Size(35, 13);
            this.b_date.TabIndex = 10;
            this.b_date.Text = "label3";
            // 
            // group
            // 
            this.group.AutoSize = true;
            this.group.Location = new System.Drawing.Point(12, 101);
            this.group.Name = "group";
            this.group.Size = new System.Drawing.Size(35, 13);
            this.group.TabIndex = 11;
            this.group.Text = "label4";
            // 
            // cours
            // 
            this.cours.AutoSize = true;
            this.cours.Location = new System.Drawing.Point(13, 128);
            this.cours.Name = "cours";
            this.cours.Size = new System.Drawing.Size(35, 13);
            this.cours.TabIndex = 12;
            this.cours.Text = "label5";
            // 
            // input_f_name
            // 
            this.input_f_name.Location = new System.Drawing.Point(113, 13);
            this.input_f_name.Name = "input_f_name";
            this.input_f_name.Size = new System.Drawing.Size(200, 20);
            this.input_f_name.TabIndex = 13;
            // 
            // input_s_name
            // 
            this.input_s_name.Location = new System.Drawing.Point(113, 40);
            this.input_s_name.Name = "input_s_name";
            this.input_s_name.Size = new System.Drawing.Size(200, 20);
            this.input_s_name.TabIndex = 14;
            // 
            // input_b_date
            // 
            this.input_b_date.Location = new System.Drawing.Point(113, 67);
            this.input_b_date.Name = "input_b_date";
            this.input_b_date.Size = new System.Drawing.Size(200, 20);
            this.input_b_date.TabIndex = 15;
            // 
            // input_group
            // 
            this.input_group.Location = new System.Drawing.Point(113, 95);
            this.input_group.Name = "input_group";
            this.input_group.Size = new System.Drawing.Size(200, 20);
            this.input_group.TabIndex = 16;
            // 
            // input_cours
            // 
            this.input_cours.Location = new System.Drawing.Point(113, 121);
            this.input_cours.Name = "input_cours";
            this.input_cours.Size = new System.Drawing.Size(200, 20);
            this.input_cours.TabIndex = 17;
            // 
            // text_res
            // 
            this.text_res.Location = new System.Drawing.Point(329, 13);
            this.text_res.Multiline = true;
            this.text_res.Name = "text_res";
            this.text_res.ReadOnly = true;
            this.text_res.Size = new System.Drawing.Size(186, 128);
            this.text_res.TabIndex = 18;
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(329, 166);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 19;
            this.btn_ok.Text = "Ok";
            this.btn_ok.UseVisualStyleBackColor = true;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(440, 166);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 20;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 222);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.text_res);
            this.Controls.Add(this.input_cours);
            this.Controls.Add(this.input_group);
            this.Controls.Add(this.input_b_date);
            this.Controls.Add(this.input_s_name);
            this.Controls.Add(this.input_f_name);
            this.Controls.Add(this.cours);
            this.Controls.Add(this.group);
            this.Controls.Add(this.b_date);
            this.Controls.Add(this.s_name);
            this.Controls.Add(this.f_name);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Лабораторна робота";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label f_name;
        private System.Windows.Forms.Label s_name;
        private System.Windows.Forms.Label b_date;
        private System.Windows.Forms.Label group;
        private System.Windows.Forms.Label cours;
        private System.Windows.Forms.TextBox input_f_name;
        private System.Windows.Forms.TextBox input_s_name;
        private System.Windows.Forms.TextBox input_b_date;
        private System.Windows.Forms.TextBox input_group;
        private System.Windows.Forms.TextBox input_cours;
        private System.Windows.Forms.TextBox text_res;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_close;
    }
}

