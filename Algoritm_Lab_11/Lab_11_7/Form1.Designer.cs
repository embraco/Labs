﻿
namespace Lab_11_7
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.fm_tv_panel = new System.Windows.Forms.TreeView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.fm_tss_owner = new System.Windows.Forms.ToolStripStatusLabel();
            this.fm_l_error = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fm_tv_panel
            // 
            this.fm_tv_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fm_tv_panel.Location = new System.Drawing.Point(2, 3);
            this.fm_tv_panel.Name = "fm_tv_panel";
            this.fm_tv_panel.Size = new System.Drawing.Size(280, 310);
            this.fm_tv_panel.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fm_tss_owner});
            this.statusStrip1.Location = new System.Drawing.Point(0, 339);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(284, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // fm_tss_owner
            // 
            this.fm_tss_owner.Name = "fm_tss_owner";
            this.fm_tss_owner.Size = new System.Drawing.Size(28, 17);
            this.fm_tss_owner.Text = "Ім\'я";
            // 
            // fm_l_error
            // 
            this.fm_l_error.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.fm_l_error.AutoSize = true;
            this.fm_l_error.Location = new System.Drawing.Point(2, 323);
            this.fm_l_error.Name = "fm_l_error";
            this.fm_l_error.Size = new System.Drawing.Size(33, 13);
            this.fm_l_error.TabIndex = 2;
            this.fm_l_error.Text = "errors";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 361);
            this.Controls.Add(this.fm_l_error);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.fm_tv_panel);
            this.MinimumSize = new System.Drawing.Size(300, 400);
            this.Name = "Form1";
            this.Text = "Form1";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView fm_tv_panel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel fm_tss_owner;
        private System.Windows.Forms.Label fm_l_error;
    }
}

