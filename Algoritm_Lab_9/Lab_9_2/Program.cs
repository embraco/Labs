﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySDK;

namespace Lab_9_2
{
    /// <summary>
    /// 2. Лінійний пошук. Створити у коді одновимірний масив з 20
    /// додатних і від’ємних цілих чисел.Знайти кількість від’ємних
    /// елементів у масиві.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal terminal = new CTerminal(120, 10);
            const int _count_elem = 20;
            int[] _arr = new int[_count_elem];
            Random _rnd = new Random();
            for (int _i = 0; _i < _arr.Length; _i++)
            {
                _arr[_i] = _rnd.Next(-9, 9);
            }

            terminal.PrintAt(0, 0, "Массив елементов");
            int i = 0;

            for (int _x = 0; _x < _count_elem; _x++)
            {
                terminal.PrintAt(_x * 4, 1, $"{_arr[i]}");
                i++;
            }

            terminal.PrintAt(0, 3, $"Количество отрицательных елементов в массиве: {Find(_arr)}");

            Console.ReadKey();
            terminal.Close();
        }

        private static int Find(int[] _arr)
        {
            int _ret = 0;
            for(int _i = 0; _i < _arr.Length; _i++)
            {
                if (_arr[_i] < 0)
                    _ret++;
            }
            return _ret;
        }
    }
}
