﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MySDK;

namespace Lab_9_1
{
    /// <summary>
    /// 1. Лінійний пошук. Згенерувати одновимірний масив 50
    /// випадкових цілих чисел.Ввести з консолі шуканий елемент.
    /// Знайти перше входження шуканого елементу у масиві.
    /// Потім в окремому циклі знайти останнє входження елементу.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal terminal = new CTerminal(120, 10);
            int[] _arr = new int[50];
            Random _rnd = new Random();
            for(int _i = 0; _i < _arr.Length; _i++)
            {
                _arr[_i] = _rnd.Next(0, 99);
            }

            terminal.PrintAt(0, 0, "Массив елементов");
            int i = 0;
            for(int _y = 1; _y < 3; _y++)
            {
                for(int _x = 0; _x < 25; _x++)
                {
                    terminal.PrintAt(_x * 4, _y, $"{_arr[i]}");
                    i++;
                }
            }
            terminal.PrintAt(0,5, "Введите искомое число:");
            terminal.SetInputMask(new Regex(@"\d"));
            int _elem;
            int.TryParse(terminal.InputAt(22, 5, 2), out _elem); terminal.PrintAt(22, 5, $"{_elem}");//альфа библиотека стирает ввод по окончании
            int _find_f = LinearSearchFirst(_arr, _elem);
            int _find_l = LinearSearchLast(_arr, _elem);
            string _first = _find_f == -1 ? "не найдено" : $"{_find_f}";
            string _last = _find_l == -1 ? "не найдено" : $"{_find_l}";
            terminal.PrintAt(0, 6, $"Искомый елемент: {_elem} первое вхождение в масиве {_first} последнее {_last}");

            Console.ReadKey();
            terminal.Close();
        }

        private static int LinearSearchFirst(int[] _arr, int _value)
        {
            int _res = -1;
            for(int _i = 0; _i < _arr.Length; _i++)
            {
                if (_arr[_i] == _value)
                    return _i;
            }
            return _res;
        }

        private static int LinearSearchLast(int[] _arr, int _value)
        {
            int _res = -1;
            for (int _i = 0; _i < _arr.Length; _i++)
            {
                if (_arr[_i] == _value)
                    _res = _i;
            }
            return _res;
        }
    }
}
