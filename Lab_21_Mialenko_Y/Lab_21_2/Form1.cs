﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySDK;

namespace Lab_21_2
{
    /// <summary>
    /// 2. Модифікувати попередню програму, реалізувавши вибір
    /// морозива через CheckBox, щоб було можна замовляти різні сорти
    /// одночасно.
    /// </summary>
    public partial class Form1 : Form
    {
        private double[] mp_price;
        public Form1()
        {
            InitializeComponent();
            this.Load += FLoad;
            fm_b_calculate.Click += FCalculate;
            fm_b_cancel.Click += FCancel;
            fm_b_exit.Click += FExit;

            mp_price = new double[4] { 10.1, 20.2, 30.3, 40.4 };
        }

        private void FLoad(object s, EventArgs e)
        {
            //инициализация переменных формы
            CRM _rm = new CRM();
            fm_status_name.Text = _rm.GetRecource("owner");
            this.Text = "Лабораторна робота №21";

            FInit();
        }

        private void FInit()
        {
            fm_cb_acream_sort_1.Text = "Ванільне";
            fm_cb_acream_sort_2.Text = "Кремове";
            fm_cb_acream_sort_3.Text = "Шоколадне";
            fm_cb_acream_sort_4.Text = "Фруктове";

            fm_cb_acream_sort_1.Checked = false;
            fm_cb_acream_sort_2.Checked = false;
            fm_cb_acream_sort_3.Checked = false;
            fm_cb_acream_sort_4.Checked = false;

            fm_l_price_sort_1.Text = mp_price[0].ToString();
            fm_l_price_sort_2.Text = mp_price[1].ToString();
            fm_l_price_sort_3.Text = mp_price[2].ToString();
            fm_l_price_sort_4.Text = mp_price[3].ToString();

            fm_num_count_sort_1.Value = 0;
            fm_num_count_sort_2.Value = 0;
            fm_num_count_sort_3.Value = 0;
            fm_num_count_sort_4.Value = 0;

            fm_l_to_price.Text = "0.0";
            fm_l_is_discount.Text = "";
        }

        private void FCalculate(object s, EventArgs e)
        {
            double _price = 0.0;
            int _discount = 0;

            _discount += (int)fm_num_count_sort_1.Value;
            _price += fm_cb_acream_sort_1.Checked ? (double)fm_num_count_sort_1.Value * mp_price[0] : 0.0;

            _discount += (int)fm_num_count_sort_2.Value;
            _price += fm_cb_acream_sort_2.Checked ? (double)fm_num_count_sort_2.Value * mp_price[1] : 0.0;

            _discount += (int)fm_num_count_sort_3.Value;
            _price += fm_cb_acream_sort_3.Checked ? (double)fm_num_count_sort_3.Value * mp_price[2] : 0.0;

            _discount += (int)fm_num_count_sort_4.Value;
            _price += fm_cb_acream_sort_4.Checked ? (double)fm_num_count_sort_4.Value * mp_price[3] : 0.0;

            fm_l_is_discount.Text = _discount >= 20 ? $"знижка 5% -{_price * 0.05}" : "";
            _price = _discount >= 20 ? _price - (_price * 0.05) : _price;
            fm_l_to_price.Text = _price.ToString();
        }

        private void FCancel(object s, EventArgs e)
        {
            FInit();
        }

        private void FExit(object s, EventArgs e)
        {
            Close();
        }
    }
}
