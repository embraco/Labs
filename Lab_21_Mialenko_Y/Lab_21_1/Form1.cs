﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySDK;

namespace Lab_21_1
{
    /// <summary>
    /// 1. Написати програму розрахунку вартості замовлення морозива.
    /// Вхідні дані для розрахунку – сорт морозива(до 4-х найменувань),
    /// кількість порцій.Вибір сорту реалізувати через RadioButton. Якщо
    /// кількість замовлень більше 20, то надається знижка 5%.
    /// </summary>
    public partial class Form1 : Form
    {
        private double[] mp_price;
        public Form1()
        {
            InitializeComponent();
            this.Load += FLoad;
            fm_b_calculate.Click += FCalculate;
            fm_b_cancel.Click += FCancel;
            fm_b_exit.Click += FExit;

            mp_price = new double[4] { 10.1, 20.2, 30.3, 40.4 };
        }

        private void FLoad(object s, EventArgs e)
        {
            //инициализация переменных формы
            CRM _rm = new CRM();
            fm_status_name.Text = _rm.GetRecource("owner");
            this.Text = "Лабораторна робота №21";

            FInit();
        }

        private void FInit()
        {
            fm_rb_acream_sort_1.Checked = true;
            fm_rb_acream_sort_1.Text = "Ванільне";
            fm_rb_acream_sort_2.Text = "Кремове";
            fm_rb_acream_sort_3.Text = "Шоколадне";
            fm_rb_acream_sort_4.Text = "Фруктове";

            fm_l_price_sort_1.Text = mp_price[0].ToString();
            fm_l_price_sort_2.Text = mp_price[1].ToString();
            fm_l_price_sort_3.Text = mp_price[2].ToString();
            fm_l_price_sort_4.Text = mp_price[3].ToString();

            fm_num_count_sort_1.Value = 0;
            fm_num_count_sort_2.Value = 0;
            fm_num_count_sort_3.Value = 0;
            fm_num_count_sort_4.Value = 0;

            fm_l_to_price.Text = "0.0";
            fm_l_is_discount.Text = "";
        }

        private void FCalculate(object s, EventArgs e)
        {
            int _index_sort =
                fm_rb_acream_sort_1.Checked ? 0 :
                fm_rb_acream_sort_2.Checked ? 1 :
                fm_rb_acream_sort_3.Checked ? 2 :
                fm_rb_acream_sort_4.Checked ? 3 : -1;
            double _price = 0.0;
            bool _discount = false;
            switch (_index_sort)
            {
                case 0:
                    _discount = fm_num_count_sort_1.Value >= 20 ? true : false;
                    _price = (double)fm_num_count_sort_1.Value * mp_price[0];
                    break;
                case 1:
                    _discount = fm_num_count_sort_2.Value >= 20 ? true : false;
                    _price = (double)fm_num_count_sort_2.Value * mp_price[1];
                    break;
                case 2:
                    _discount = fm_num_count_sort_3.Value >= 20 ? true : false;
                    _price = (double)fm_num_count_sort_3.Value * mp_price[2];
                    break;
                case 3:
                    _discount = fm_num_count_sort_4.Value >= 20 ? true : false;
                    _price = (double)fm_num_count_sort_4.Value * mp_price[3];
                    break;
            }
            fm_l_is_discount.Text = _discount ? $"знижка 5% -{_price * 0.05}" : "";
            _price = _discount ? _price - (_price * 0.05) : _price;
            fm_l_to_price.Text = _price.ToString();
        }

        private void FCancel(object s, EventArgs e)
        {
            FInit();
        }

        private void FExit(object s, EventArgs e)
        {
            Close();
        }
    }
}
