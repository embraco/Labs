﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabCommon;
using MySDK;

namespace Lab_11_2
{
    /// <summary>
    /// 2. Створити дерево структури МНТУ. Додати до вузла кафедри КНІС
    /// вузол Групи.До цього вузла додати 2 вузли: К-71, ПІ-71.
    /// </summary>
    class Program
    {//iI
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(60, 14);
            _terminal.Title = "Лабораторная работа №11.2";

            CTree<string> _tree =
                new CTree<string>("МНТУ",
                    new CTree<string>("Департаменти"),
                    new CTree<string>("Факультет КННI",
                        new CTree<string>("кафедра КНIС",
                            new CTree<string>("Группи",
                                new CTree<string>("К-71"),
                                new CTree<string>("ПI-71"))),
                        new CTree<string>("кафедра метематики")),
                    new CTree<string>("Факультет економiки i менеджменту",
                        new CTree<string>("кафедра мiжнародноi економiки"),
                        new CTree<string>("кафедра Облiк i аудит")),
                    new CTree<string>("Факультет фiзреабiлiтацii"));

            List<string> _string_tree = _tree.GetStringTree();
            for (int i = 0; i < _string_tree.Count; i++)
            {
                _terminal.PrintAt(0, i, _string_tree[i]);
            }

            Console.ReadKey();
            _terminal.Close();
        }
    }
}
