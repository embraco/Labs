﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabCommon;
using MySDK;

namespace Lab_11_3
{
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(60, 24);
            _terminal.Title = "Лабораторная работа №11.3";

            CLTree<string> _root = new CLTree<string>("Товари");
            {
                CLTree<string> _lvl_1_1 = _root.AddChild("Комп'ютерна технiка");
                {
                    CLTree<string> _lvl_2_1 = _lvl_1_1.AddChild("ПК");
                    {
                        _lvl_2_1.AddChild("Модель ПК 1");
                        _lvl_2_1.AddChild("Модель ПК 2");
                        _lvl_2_1.AddChild("Модель ПК 3");
                    }
                    CLTree<string> _lvl_2_2 = _lvl_1_1.AddChild("Ноутбуки");
                    {
                        _lvl_2_2.AddChild("Модель Ноутбук 1");
                        _lvl_2_2.AddChild("Модель Ноутбук 2");
                        _lvl_2_2.AddChild("Модель Ноутбук 3");
                    }
                }
                CLTree<string> _lvl_1_2 = _root.AddChild("Принтери");
                {
                    CLTree<string> _lvl_2_1 = _lvl_1_2.AddChild("Лазернi");
                    {
                        _lvl_2_1.AddChild("Модель Лазерний принтер 1");
                        _lvl_2_1.AddChild("Модель Лазерний принтер 2");
                        _lvl_2_1.AddChild("Модель Лазерний принтер 3");
                    }
                    CLTree<string> _lvl_2_2 = _lvl_1_2.AddChild("Струменевi");
                    {
                        _lvl_2_2.AddChild("Модель Струменевий принтер 1");
                        _lvl_2_2.AddChild("Модель Струменевий принтер 2");
                        _lvl_2_2.AddChild("Модель Струменевий принтер 3");
                    }
                }
            }

            List<string> _string_tree = _root.GetListStringTree();
            for (int i = 0; i < _string_tree.Count; i++)
            {
                _terminal.PrintAt(0, i, _string_tree[i]);
            }

            Console.ReadKey();
            _terminal.Close();
        }
    }
}
