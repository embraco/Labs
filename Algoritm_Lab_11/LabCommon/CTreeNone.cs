﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabCommon
{
    public class CTreeNone<T>
    {
        //внутренние поля класса
        private T m_value;
        private bool m_hasParent;
        private List<CTreeNone<T>> m_children;

        //конструктор
        public CTreeNone(T _value)
        {
            if (_value == null)
                throw new ArgumentNullException("Не может содержать null");
            m_value = _value;
            m_children = new List<CTreeNone<T>>();
        }

        //свойства класса
        public T Value
        {
            get { return m_value; }
            set { m_value = value; }
        }

        public int ChildrenCount => m_children.Count;

        //методы класса >>интерфейс<<
        public void AddChild(CTreeNone<T> _value)
        {
            if (_value == null)
                throw new ArgumentNullException("Не может содержать null");
            if (_value.m_hasParent)
                throw new ArgumentException("node уже имеет родителя");
            _value.m_hasParent = true;
            m_children.Add(_value);
        }

        //нужно исследовать актуальность использования индексатора в данном случае
        public CTreeNone<T> this[int _index]
        {
            get { return m_children[_index];}
            
        }
    }
}
