﻿
namespace Lab_21_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.fm_status_lable = new System.Windows.Forms.ToolStripStatusLabel();
            this.fm_status_name = new System.Windows.Forms.ToolStripStatusLabel();
            this.fm_cb_acream_sort_1 = new System.Windows.Forms.CheckBox();
            this.fm_cb_acream_sort_2 = new System.Windows.Forms.CheckBox();
            this.fm_cb_acream_sort_3 = new System.Windows.Forms.CheckBox();
            this.fm_cb_acream_sort_4 = new System.Windows.Forms.CheckBox();
            this.fm_num_count_sort_1 = new System.Windows.Forms.NumericUpDown();
            this.fm_num_count_sort_2 = new System.Windows.Forms.NumericUpDown();
            this.fm_num_count_sort_3 = new System.Windows.Forms.NumericUpDown();
            this.fm_num_count_sort_4 = new System.Windows.Forms.NumericUpDown();
            this.fm_l_price_sort_1 = new System.Windows.Forms.Label();
            this.fm_l_price_sort_2 = new System.Windows.Forms.Label();
            this.fm_l_price_sort_3 = new System.Windows.Forms.Label();
            this.fm_l_price_sort_4 = new System.Windows.Forms.Label();
            this.fm_lable_sort = new System.Windows.Forms.Label();
            this.fm_lable_count = new System.Windows.Forms.Label();
            this.fm_lable_price = new System.Windows.Forms.Label();
            this.fm_b_calculate = new System.Windows.Forms.Button();
            this.fm_b_cancel = new System.Windows.Forms.Button();
            this.fm_b_exit = new System.Windows.Forms.Button();
            this.fm_lable_to_price = new System.Windows.Forms.Label();
            this.fm_l_to_price = new System.Windows.Forms.Label();
            this.fm_l_is_discount = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_4)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fm_status_lable,
            this.fm_status_name});
            this.statusStrip1.Location = new System.Drawing.Point(0, 254);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(316, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // fm_status_lable
            // 
            this.fm_status_lable.Name = "fm_status_lable";
            this.fm_status_lable.Size = new System.Drawing.Size(56, 17);
            this.fm_status_lable.Text = "Виконав:";
            // 
            // fm_status_name
            // 
            this.fm_status_name.Name = "fm_status_name";
            this.fm_status_name.Size = new System.Drawing.Size(28, 17);
            this.fm_status_name.Text = "Ім\'я";
            // 
            // fm_rb_acream_sort_1
            // 
            this.fm_cb_acream_sort_1.AutoSize = true;
            this.fm_cb_acream_sort_1.Location = new System.Drawing.Point(17, 35);
            this.fm_cb_acream_sort_1.Name = "fm_rb_acream_sort_1";
            this.fm_cb_acream_sort_1.Size = new System.Drawing.Size(68, 17);
            this.fm_cb_acream_sort_1.TabIndex = 1;
            this.fm_cb_acream_sort_1.Text = "сорт №1";
            this.fm_cb_acream_sort_1.UseVisualStyleBackColor = true;
            // 
            // fm_rb_acream_sort_2
            // 
            this.fm_cb_acream_sort_2.AutoSize = true;
            this.fm_cb_acream_sort_2.Location = new System.Drawing.Point(17, 59);
            this.fm_cb_acream_sort_2.Name = "fm_rb_acream_sort_2";
            this.fm_cb_acream_sort_2.Size = new System.Drawing.Size(68, 17);
            this.fm_cb_acream_sort_2.TabIndex = 2;
            this.fm_cb_acream_sort_2.Text = "сорт №2";
            this.fm_cb_acream_sort_2.UseVisualStyleBackColor = true;
            // 
            // fm_rb_acream_sort_3
            // 
            this.fm_cb_acream_sort_3.AutoSize = true;
            this.fm_cb_acream_sort_3.Location = new System.Drawing.Point(17, 83);
            this.fm_cb_acream_sort_3.Name = "fm_rb_acream_sort_3";
            this.fm_cb_acream_sort_3.Size = new System.Drawing.Size(68, 17);
            this.fm_cb_acream_sort_3.TabIndex = 3;
            this.fm_cb_acream_sort_3.Text = "сорт №3";
            this.fm_cb_acream_sort_3.UseVisualStyleBackColor = true;
            // 
            // fm_rb_acream_sort_4
            // 
            this.fm_cb_acream_sort_4.AutoSize = true;
            this.fm_cb_acream_sort_4.Location = new System.Drawing.Point(17, 107);
            this.fm_cb_acream_sort_4.Name = "fm_rb_acream_sort_4";
            this.fm_cb_acream_sort_4.Size = new System.Drawing.Size(68, 17);
            this.fm_cb_acream_sort_4.TabIndex = 4;
            this.fm_cb_acream_sort_4.Text = "сорт №4";
            this.fm_cb_acream_sort_4.UseVisualStyleBackColor = true;
            // 
            // fm_num_count_sort_1
            // 
            this.fm_num_count_sort_1.Location = new System.Drawing.Point(127, 29);
            this.fm_num_count_sort_1.Name = "fm_num_count_sort_1";
            this.fm_num_count_sort_1.Size = new System.Drawing.Size(44, 20);
            this.fm_num_count_sort_1.TabIndex = 5;
            // 
            // fm_num_count_sort_2
            // 
            this.fm_num_count_sort_2.Location = new System.Drawing.Point(127, 55);
            this.fm_num_count_sort_2.Name = "fm_num_count_sort_2";
            this.fm_num_count_sort_2.Size = new System.Drawing.Size(44, 20);
            this.fm_num_count_sort_2.TabIndex = 6;
            // 
            // fm_num_count_sort_3
            // 
            this.fm_num_count_sort_3.Location = new System.Drawing.Point(127, 81);
            this.fm_num_count_sort_3.Name = "fm_num_count_sort_3";
            this.fm_num_count_sort_3.Size = new System.Drawing.Size(44, 20);
            this.fm_num_count_sort_3.TabIndex = 7;
            // 
            // fm_num_count_sort_4
            // 
            this.fm_num_count_sort_4.Location = new System.Drawing.Point(127, 107);
            this.fm_num_count_sort_4.Name = "fm_num_count_sort_4";
            this.fm_num_count_sort_4.Size = new System.Drawing.Size(44, 20);
            this.fm_num_count_sort_4.TabIndex = 8;
            // 
            // fm_l_price_sort_1
            // 
            this.fm_l_price_sort_1.AutoSize = true;
            this.fm_l_price_sort_1.Location = new System.Drawing.Point(224, 31);
            this.fm_l_price_sort_1.Name = "fm_l_price_sort_1";
            this.fm_l_price_sort_1.Size = new System.Drawing.Size(29, 13);
            this.fm_l_price_sort_1.TabIndex = 9;
            this.fm_l_price_sort_1.Text = "Ціна";
            // 
            // fm_l_price_sort_2
            // 
            this.fm_l_price_sort_2.AutoSize = true;
            this.fm_l_price_sort_2.Location = new System.Drawing.Point(224, 57);
            this.fm_l_price_sort_2.Name = "fm_l_price_sort_2";
            this.fm_l_price_sort_2.Size = new System.Drawing.Size(29, 13);
            this.fm_l_price_sort_2.TabIndex = 10;
            this.fm_l_price_sort_2.Text = "Ціна";
            // 
            // fm_l_price_sort_3
            // 
            this.fm_l_price_sort_3.AutoSize = true;
            this.fm_l_price_sort_3.Location = new System.Drawing.Point(224, 83);
            this.fm_l_price_sort_3.Name = "fm_l_price_sort_3";
            this.fm_l_price_sort_3.Size = new System.Drawing.Size(29, 13);
            this.fm_l_price_sort_3.TabIndex = 11;
            this.fm_l_price_sort_3.Text = "Ціна";
            // 
            // fm_l_price_sort_4
            // 
            this.fm_l_price_sort_4.AutoSize = true;
            this.fm_l_price_sort_4.Location = new System.Drawing.Point(224, 109);
            this.fm_l_price_sort_4.Name = "fm_l_price_sort_4";
            this.fm_l_price_sort_4.Size = new System.Drawing.Size(29, 13);
            this.fm_l_price_sort_4.TabIndex = 12;
            this.fm_l_price_sort_4.Text = "Ціна";
            // 
            // fm_lable_sort
            // 
            this.fm_lable_sort.AutoSize = true;
            this.fm_lable_sort.Location = new System.Drawing.Point(14, 9);
            this.fm_lable_sort.Name = "fm_lable_sort";
            this.fm_lable_sort.Size = new System.Drawing.Size(84, 13);
            this.fm_lable_sort.TabIndex = 13;
            this.fm_lable_sort.Text = "Сорт морозива";
            // 
            // fm_lable_count
            // 
            this.fm_lable_count.AutoSize = true;
            this.fm_lable_count.Location = new System.Drawing.Point(124, 9);
            this.fm_lable_count.Name = "fm_lable_count";
            this.fm_lable_count.Size = new System.Drawing.Size(53, 13);
            this.fm_lable_count.TabIndex = 14;
            this.fm_lable_count.Text = "Кількість";
            // 
            // fm_lable_price
            // 
            this.fm_lable_price.AutoSize = true;
            this.fm_lable_price.Location = new System.Drawing.Point(224, 9);
            this.fm_lable_price.Name = "fm_lable_price";
            this.fm_lable_price.Size = new System.Drawing.Size(29, 13);
            this.fm_lable_price.TabIndex = 15;
            this.fm_lable_price.Text = "Ціна";
            // 
            // fm_b_calculate
            // 
            this.fm_b_calculate.Location = new System.Drawing.Point(15, 179);
            this.fm_b_calculate.Name = "fm_b_calculate";
            this.fm_b_calculate.Size = new System.Drawing.Size(75, 23);
            this.fm_b_calculate.TabIndex = 16;
            this.fm_b_calculate.Text = "Замовити";
            this.fm_b_calculate.UseVisualStyleBackColor = true;
            // 
            // fm_b_cancel
            // 
            this.fm_b_cancel.Location = new System.Drawing.Point(102, 179);
            this.fm_b_cancel.Name = "fm_b_cancel";
            this.fm_b_cancel.Size = new System.Drawing.Size(75, 23);
            this.fm_b_cancel.TabIndex = 17;
            this.fm_b_cancel.Text = "Відмінити";
            this.fm_b_cancel.UseVisualStyleBackColor = true;
            // 
            // fm_b_exit
            // 
            this.fm_b_exit.Location = new System.Drawing.Point(192, 179);
            this.fm_b_exit.Name = "fm_b_exit";
            this.fm_b_exit.Size = new System.Drawing.Size(75, 23);
            this.fm_b_exit.TabIndex = 18;
            this.fm_b_exit.Text = "Вийти";
            this.fm_b_exit.UseVisualStyleBackColor = true;
            // 
            // fm_lable_to_price
            // 
            this.fm_lable_to_price.AutoSize = true;
            this.fm_lable_to_price.Location = new System.Drawing.Point(18, 146);
            this.fm_lable_to_price.Name = "fm_lable_to_price";
            this.fm_lable_to_price.Size = new System.Drawing.Size(60, 13);
            this.fm_lable_to_price.TabIndex = 19;
            this.fm_lable_to_price.Text = "До сплати";
            // 
            // fm_l_to_price
            // 
            this.fm_l_to_price.AutoSize = true;
            this.fm_l_to_price.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fm_l_to_price.Location = new System.Drawing.Point(96, 144);
            this.fm_l_to_price.Name = "fm_l_to_price";
            this.fm_l_to_price.Size = new System.Drawing.Size(59, 15);
            this.fm_l_to_price.TabIndex = 20;
            this.fm_l_to_price.Text = "до сплати";
            // 
            // fm_l_is_discount
            // 
            this.fm_l_is_discount.AutoSize = true;
            this.fm_l_is_discount.Location = new System.Drawing.Point(172, 146);
            this.fm_l_is_discount.Name = "fm_l_is_discount";
            this.fm_l_is_discount.Size = new System.Drawing.Size(45, 13);
            this.fm_l_is_discount.TabIndex = 21;
            this.fm_l_is_discount.Text = "знижка";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 276);
            this.Controls.Add(this.fm_l_is_discount);
            this.Controls.Add(this.fm_l_to_price);
            this.Controls.Add(this.fm_lable_to_price);
            this.Controls.Add(this.fm_b_exit);
            this.Controls.Add(this.fm_b_cancel);
            this.Controls.Add(this.fm_b_calculate);
            this.Controls.Add(this.fm_lable_price);
            this.Controls.Add(this.fm_lable_count);
            this.Controls.Add(this.fm_lable_sort);
            this.Controls.Add(this.fm_l_price_sort_4);
            this.Controls.Add(this.fm_l_price_sort_3);
            this.Controls.Add(this.fm_l_price_sort_2);
            this.Controls.Add(this.fm_l_price_sort_1);
            this.Controls.Add(this.fm_num_count_sort_4);
            this.Controls.Add(this.fm_num_count_sort_3);
            this.Controls.Add(this.fm_num_count_sort_2);
            this.Controls.Add(this.fm_num_count_sort_1);
            this.Controls.Add(this.fm_cb_acream_sort_4);
            this.Controls.Add(this.fm_cb_acream_sort_3);
            this.Controls.Add(this.fm_cb_acream_sort_2);
            this.Controls.Add(this.fm_cb_acream_sort_1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fm_num_count_sort_4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel fm_status_lable;
        private System.Windows.Forms.ToolStripStatusLabel fm_status_name;
        private System.Windows.Forms.CheckBox fm_cb_acream_sort_1;
        private System.Windows.Forms.CheckBox fm_cb_acream_sort_2;
        private System.Windows.Forms.CheckBox fm_cb_acream_sort_3;
        private System.Windows.Forms.CheckBox fm_cb_acream_sort_4;
        private System.Windows.Forms.NumericUpDown fm_num_count_sort_1;
        private System.Windows.Forms.NumericUpDown fm_num_count_sort_2;
        private System.Windows.Forms.NumericUpDown fm_num_count_sort_3;
        private System.Windows.Forms.NumericUpDown fm_num_count_sort_4;
        private System.Windows.Forms.Label fm_l_price_sort_1;
        private System.Windows.Forms.Label fm_l_price_sort_2;
        private System.Windows.Forms.Label fm_l_price_sort_3;
        private System.Windows.Forms.Label fm_l_price_sort_4;
        private System.Windows.Forms.Label fm_lable_sort;
        private System.Windows.Forms.Label fm_lable_count;
        private System.Windows.Forms.Label fm_lable_price;
        private System.Windows.Forms.Button fm_b_calculate;
        private System.Windows.Forms.Button fm_b_cancel;
        private System.Windows.Forms.Button fm_b_exit;
        private System.Windows.Forms.Label fm_lable_to_price;
        private System.Windows.Forms.Label fm_l_to_price;
        private System.Windows.Forms.Label fm_l_is_discount;
    }
}

