﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySDK;

namespace Lab_10_6
{
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(120, 26);
            _terminal.Title = "Лабораторна робота №10.5";

            List<string> _list_one = new List<string>() {
                "История Украины",
                "Физика",
                "Физкультура",
                "Дискретная математика",
                "Основы программирования",
                "Основы компьютерной графики",
                "Введение в специальность",
                "Английский язык"
            };
            List<string> _list_thwo = new List<string>(){
                "Физкультура",
                "Дискретная математика",
                "Математический анализ",
                "Объектно ориентированное прог",
                "Английский язык",
                "Дискретные структуры",
                "Числительные методы",
                "Алгоритмы и структуры данных",
            }; ;


            _terminal.PrintAt(0, 0, "Перша множина");
            _terminal.PrintAt(30, 0, "Друга множина");
            _terminal.PrintAt(0, 10, "Об'єднання множин");
            _terminal.PrintAt(30, 10, "Перетин множин");
            _terminal.PrintAt(60, 10, "Рiзниця множин");
            var _list_except = _list_one.Except(_list_thwo);
            var _list_inter = _list_one.Intersect(_list_thwo);
            var _list_unit = _list_one.Union(_list_thwo);

            for (int i = 0; i < 16; i++)
            {
                if(_list_one.Count > i)
                    _terminal.PrintAt(0, 1 + i, _list_one[i]);
                if(_list_thwo.Count > i)
                    _terminal.PrintAt(30, 1 + i, _list_thwo[i]);
                if(_list_unit.Count() > i)
                    _terminal.PrintAt(0, 11 + i, _list_unit.ElementAt(i));
                if(_list_inter.Count() > i)
                    _terminal.PrintAt(30, 11 + i, _list_inter.ElementAt(i));
                if(_list_except.Count() > i)
                    _terminal.PrintAt(60, 11 + i, _list_except.ElementAt(i));
            }
            
            Console.ReadKey();
            _terminal.Close();
        }
    }
}
