﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySDK;

namespace Lab_11_7
{
    /// <summary>
    /// 7. TreeView. Створити проект Windows Form. Реалізувати обхід каталогів
    /// файлової системи.
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            fm_l_error.Text = "";
            this.SizeChanged += (s, e) => { fm_tv_panel.Width = Width - 20; fm_tv_panel.Height = Height - 90; };
            Load += EventLoad;
            fm_tv_panel.AfterSelect += EventAfterSelect;
        }

        private void EventAfterSelect(object s, TreeViewEventArgs e)
        {
            try
            {
                if (!Directory.Exists(e.Node.FullPath))
                    return;
                string[] dir = Directory.GetDirectories(e.Node.FullPath);
                foreach (string _d in dir)
                    e.Node.Nodes.Add(new TreeNode { Text = _d.Remove(0, _d.LastIndexOf("\\") + 1) });
            }
            catch (Exception ex) { fm_l_error.Text = ex.Message; }
        }

        private void EventLoad(object sender, EventArgs e)
        {
            CRM rm = new CRM();
            fm_tss_owner.Text = rm.GetRecource("owner");
            Text = "Лабораторная работа №11.7";
            foreach (DriveInfo _d in DriveInfo.GetDrives())
                fm_tv_panel.Nodes.Add(new TreeNode { Text = _d.Name });
        }
    }
}