﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySDK;

namespace Lab_11_5
{
    public partial class Form1 : Form
    {
        private List<string> _me_81;
        private List<string> _fn_71;
        public Form1()
        {
            InitializeComponent();
            fm_tv_list.AfterSelect += EventAfterSelect;
            fm_b_add.Click += EventButtonAdd;
            fm_b_delete.Click += EventButtonDel;
            fm_b_exit.Click += (s, e) => { Close(); };
            this.Load += EventLoad;
            fm_b_list.Click += EventButtonList;
        }

        private void EventButtonList(object s, EventArgs e)
        {
            fm_lb_list.Items.Clear();
            GetStringListDir(fm_tv_list.Nodes[0], 0);
        }
        private void GetStringListDir(TreeNode _nod, int _depth)
        {
            fm_lb_list.Items.Add($"{new string(' ', _depth * 2)}{_depth}:{_nod.Text}");
            TreeNodeCollection _child = _nod.Nodes;
            foreach (TreeNode _n in _child)
                GetStringListDir(_n, _depth + 1);
        }
        private void EventLoad(object s, EventArgs e)
        {
            _me_81 = new List<string>();
            _me_81.Add("Ім'я Прізвище 1 МЕ-81");
            _me_81.Add("Ім'я Прізвище 2 МЕ-81");
            _me_81.Add("Ім'я Прізвище 3 МЕ-81");
            _me_81.Add("Ім'я Прізвище 4 МЕ-81");
            _me_81.Add("Ім'я Прізвище 5 МЕ-81");

            _fn_71 = new List<string>();
            _fn_71.Add("Ім'я Прізвище 1 ФН-71");
            _fn_71.Add("Ім'я Прізвище 2 ФН-71");
            _fn_71.Add("Ім'я Прізвище 3 ФН-71");
            _fn_71.Add("Ім'я Прізвище 4 ФН-71");
            _fn_71.Add("Ім'я Прізвище 5 ФН-71");

            CRM _rm = new CRM();
            fm_tlss_ovner.Text = _rm.GetRecource("owner");
            this.Text = "Лабораторная работа №11.5";
        }

        private void EventButtonDel(object s, EventArgs e)
        {
            if(fm_tv_list.SelectedNode != null)
                fm_tv_list.SelectedNode.Remove();
        }

        private void EventButtonAdd(object s, EventArgs e)
        {
            TreeNode _node = new TreeNode();
            _node.Text = fm_tb_name.Text;
            _node.Name = fm_tb_text.Text;
            fm_tv_list.SelectedNode.Nodes.Add(_node);
        }

        private void EventAfterSelect(object s, TreeViewEventArgs e)
        {
            fm_lb_list.Items.Clear();
            if (e.Node.Text == "МЕ-81")
                foreach (string _s in _me_81)
                    fm_lb_list.Items.Add(_s);
            if(e.Node.Text == "ФН-71")
                foreach (string _s in _fn_71)
                    fm_lb_list.Items.Add(_s);
        }
    }
}
