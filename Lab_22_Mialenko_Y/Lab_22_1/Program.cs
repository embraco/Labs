﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lab_22_Forms;

namespace Lab_22_1
{
    /// <summary>
    /// 1. Написати програму Реєстрація. На формі вказати Login,
    /// Password, підтвердження Password, поштовий ящик eMail.Назву
    /// поштового сервера вибрати з ComboBox.Результати вивести в
    /// текстове поле.
    /// </summary>
    static class Program
    {
        private static FLab_22_1 mf_form;
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //задаем поведение формы, и устанавливаем начальные значения
            mf_form = new FLab_22_1();
            mf_form.Exit.Click += EventExit;
            mf_form.Register.Click += EventRegister;
            mf_form.EmailDomain.Items.AddRange(new object[] { 
                                                "@ukr.net",
                                                "@gmai.com",
                                                "@mail.ru",
                                                "@istu.edu.ua"
                                                });
            mf_form.EmailDomain.SelectedIndex = 0;
            //----------------------------------------------------------

            Application.Run(mf_form);

            //Application.Run();
        }

        private static void EventRegister(object s, EventArgs e)
        {
            if(mf_form.Login.Text.Length == 0)
            {
                MessageBox.Show("Поле Login неповинно бути пустим");
                return;
            }
            if (mf_form.Password.Text.Length == 0)
            {
                MessageBox.Show("Поле Password неповинно бути пустим");
                return;
            }
            if (mf_form.Email.Text.Length == 0)
            {
                MessageBox.Show("Поле email неповинно бути пустим");
                return;
            }
            if (!char.IsLetter(mf_form.Login.Text[0]))
            {
                MessageBox.Show("Першою у логіні повинна бути літера");
                return;
            }
            if (!char.IsLetter(mf_form.Password.Text[0]))
            {
                MessageBox.Show("Першою у паролі повинна бути літера");
                return;
            }
            if(mf_form.Password.Text != mf_form.PassConf.Text)
            {
                MessageBox.Show("Паролі несовпадають");
                return;
            }
            foreach(char _c in mf_form.Email.Text)
            {
                if(_c == '@' || _c == '$')
                {
                    MessageBox.Show($"Заборонений символ у email {_c}");
                    return;
                }
            }
            string _nl = Environment.NewLine;
            mf_form.Output.Text = $"{mf_form.Login.Text}{_nl}{mf_form.Password.Text}{_nl}{mf_form.Email.Text}{mf_form.EmailDomain.SelectedItem.ToString()}";
        }
        private static void EventExit(object s, EventArgs e)
        {
            Application.Exit();
        }
    }
}
