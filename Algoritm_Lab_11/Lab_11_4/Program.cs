﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabCommon;
using MySDK;

namespace Lab_11_4
{
    /// <summary>
    /// 4. Обхід дерева каталогів (на базі приладу 4). Створити дерево для
    /// представлення структури свого проекту C# і вивести його елементи на
    /// консоль.
    /// </summary>
    class Program
    {
        private static CTerminal _terminal;
        private static int _index;
        static void Main(string[] args)
        {
            _terminal = new CTerminal(120, 24);
            _terminal.Title = "Лабораторная работа №11.4";
            string _path = "..\\Lab_11_4";
            DirectoryInfo _dir = new DirectoryInfo(_path);

            _index = 0;
            GetStringListDir(_dir, 0);

            Console.ReadKey();
            _terminal.Close();
        }

        private static void GetStringListDir(DirectoryInfo _dir, int _depth)
        {
            _terminal.PrintAt(_depth * 2, _index++, $"{_depth}:{_dir.FullName}");
            DirectoryInfo[] _child = _dir.GetDirectories();
            foreach (DirectoryInfo c in _child)
                GetStringListDir(c, _depth + 1);
        }
    }
}
