﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab_23_Forms;

namespace Lab_23_1
{
    class SCream
    {
        public string s_name;
        public double d_price;
        public Image o_img;
        public int i_count;
        public SCream(string _n, double _p, Image _img)
        {
            s_name  = _n;
            d_price = _p;
            o_img   = _img;
            i_count = 0;
        }

        public void Reset()
        {
            i_count = 0;
        }
        public override string ToString()
        {
            return $"{s_name} ціна:{d_price} кількість:{i_count}";
        }
    }
    static class Program
    {
        private static List<SCream> m_menu;
        private static int m_index;
        private static FLab_23_1 m_form;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            m_form = new FLab_23_1();
            m_form.BPrev.Click += (s, e) => { if (m_index > 0) m_index--; Update(); };
            m_form.BNext.Click += (s, e) => { if(m_index < m_menu.Count - 1)m_index++; Update(); };
            m_form.ImageButton.Click += EventAddToMenu;
            m_form.BOrder.Click += EventOrder;
            ResourceManager _rm = m_form.GetResource;

            m_menu = new List<SCream>();
            m_menu.Add(new SCream("Асорти",           23.3,  (Image)_rm.GetObject("asorti")));
            m_menu.Add(new SCream("Ванильное",        34.4,  (Image)_rm.GetObject("cream")));
            m_menu.Add(new SCream("Ескимо",           45.5,  (Image)_rm.GetObject("escimo")));
            m_menu.Add(new SCream("Фруктовое",        56.6,  (Image)_rm.GetObject("fruit")));
            m_menu.Add(new SCream("Шеколад и ваниль", 67.7,  (Image)_rm.GetObject("multi")));

            m_index = 0;

            Update();
            Application.Run(m_form);
        }

        private static void EventAddToMenu(object s, EventArgs e)
        {
            SCream _tmp = m_menu[m_index];
            if (m_form.cbOrder.Items.Contains(m_menu[m_index]))
                m_menu[m_index].i_count++;
            else
            {
                m_menu[m_index].i_count++;
                m_form.cbOrder.Items.Add(m_menu[m_index]);
            }
            //костыль на обновление списка
            m_form.cbOrder.Hide();
            m_form.cbOrder.Show();
        }

        private static void EventOrder(object s, EventArgs e)
        {
            if (m_form.cbOrder.CheckedItems.Count == 0)
                return;
            string _msg = "";
            string _nl = Environment.NewLine;
            double _all = 0;
            double _curr;
            foreach(SCream _el in m_form.cbOrder.CheckedItems)
            {
                _curr = _el.d_price * _el.i_count;
                _msg = $"{_msg}{_el.ToString()} Загалом: {_curr}{_nl}";
                _all += _curr;
                _el.Reset();
            }
            m_form.cbOrder.Items.Clear();
            _msg = $"{_msg}{_nl}Разом:{_all}";
            m_form.Order.Text = _msg;
        }

        private static void Update()
        {
            string _nl = Environment.NewLine;
            m_form.InfoIce.Text = $"Назва: {m_menu[m_index].s_name}{_nl}Ціна: {m_menu[m_index].d_price}";
            m_form.ImageButton.Image = m_menu[m_index].o_img;
        }
    }
}
