﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MySDK;

namespace Lab_9_3
{
    /// <summary>
    /// 3. Бінарний пошук. Згенерувати одновимірний масив 110
    /// випадкових цілих чисел.Відсортувати масив методом
    /// швидкого сортування.Вивести відсортований масив на
    /// консоль.Ввести з консолі шуканий елемент.Методом
    /// бінарного пошуку знайти шуканий елемент.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal terminal = new CTerminal(100, 20);
            const int _count = 110;
            int[] _arr = new int[_count];
            Random _rnd = new Random();
            for (int _i = 0; _i < _arr.Length; _i++)
            {
                _arr[_i] = _rnd.Next(0, 99);
            }

            terminal.PrintAt(0, 0, "Массив елементов не отсортированный");
            int i = 0;
            for (int _y = 1; _y < 6; _y++)
            {
                for (int _x = 0; _x < 22; _x++)
                {
                    terminal.PrintAt(_x * 4, _y, $"{_arr[i]}");
                    i++;
                }
            }

            QuickShort(_arr);
            terminal.PrintAt(0, 7, "Массив елементов отсортированный (QuickShort)");
            i = 0;
            for (int _y = 8; _y < 13; _y++)
            {
                for (int _x = 0; _x < 22; _x++)
                {
                    terminal.PrintAt(_x * 4, _y, $"{_arr[i]}");
                    i++;
                }
            }

            terminal.PrintAt(0, 16, "Введите искомое число:");
            terminal.SetInputMask(new Regex(@"\d"));
            int _elem;
            int.TryParse(terminal.InputAt(22, 16, 2), out _elem);
            terminal.PrintAt(22, 16, $"{_elem}");//альфа библиотека стирает ввод по окончании
            int _count_find_iterration = 0;
            int _index = FindBinarry(_arr, _elem, out _count_find_iterration);

            terminal.PrintAt(0, 17, $"Искомый елемент находится в : {_index}(й) позиции. Итераций поиска было {_count_find_iterration}");
            terminal.PrintAt(0, 18, "Для выхода нажмите любую клавишу");
            Console.ReadKey();
            terminal.Close();
        }

        private static void QuickShort(int[] _arr)
        {
            void QuickShortInner(int[] i_arr, int i_first, int i_last)
            {
                int _p = _arr[(i_last - i_first) / 2 + i_first];
                int _tmp;
                int _i = i_first;
                int _j = i_last;
                while (_i <= _j)
                {
                    while (_arr[_i] < _p && _i <= i_last) ++_i;
                    while (_arr[_j] > _p && _j >= i_first) --_j;
                    if (_i <= _j)
                    {
                        _tmp = _arr[_i];
                        _arr[_i] = _arr[_j];
                        _arr[_j] = _tmp;
                        ++_i;
                        --_j;
                    }
                }
                if (_j > i_first) QuickShortInner(i_arr, i_first, _j);
                if (_i < i_last) QuickShortInner(i_arr, _i, i_last);
            }

            int _first = 0;
            int _last = _arr.Length - 1;

            QuickShortInner(_arr, _first, _last);
        }

        private static int FindBinarry(int[] _arr, int _value, out int _count)
        {
            _count = 0;
            int _ret = -1;
            int _begin = 0;
            int _end = _arr.Length;
            int _c;
            while(_begin < _end)
            {
                _count++; //для дебага, количество итераций
                _c = _begin + (_end - _begin) / 2;
                if (_value < _arr[_c]) _end = _c;
                else if (_value > _arr[_c]) _begin = ++_c;
                else
                {
                    _ret = _c;
                    break;
                }
            }
            return _ret;
        }
    }
}
