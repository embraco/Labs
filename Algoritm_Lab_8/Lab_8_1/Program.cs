﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_8_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //-----------выполнение замеров------------
            int[] arr_1_50 = new int[50];
            int[] arr_2_50 = new int[50];
            int[] arr_3_50 = new int[50];

            int[] arr_1_100 = new int[100];
            int[] arr_2_100 = new int[100];
            int[] arr_3_100 = new int[100];

            int[] arr_1_200 = new int[200];
            int[] arr_2_200 = new int[200];
            int[] arr_3_200 = new int[200];

            int[] arr_1_1000 = new int[1000];
            int[] arr_2_1000 = new int[1000];
            int[] arr_3_1000 = new int[1000];

            Random rnd = new Random();
            Generic(arr_1_50, rnd);
            Generic(arr_2_50, rnd);
            Generic(arr_3_50, rnd);

            Generic(arr_1_100, rnd);
            Generic(arr_2_100, rnd);
            Generic(arr_3_100, rnd);

            Generic(arr_1_200, rnd);
            Generic(arr_2_200, rnd);
            Generic(arr_3_200, rnd);

            Generic(arr_1_1000, rnd);
            Generic(arr_2_1000, rnd);
            Generic(arr_3_1000, rnd);

            Stopwatch sw = new Stopwatch();
            TimeSpan ts_1_50, ts_1_100, ts_1_200, ts_1_1000;
            TimeSpan ts_2_50, ts_2_100, ts_2_200, ts_2_1000;
            TimeSpan ts_3_50, ts_3_100, ts_3_200, ts_3_1000;

            //сортируем 50
            Show(arr_1_50);
            sw.Restart();
            Array.Sort(arr_1_50);
            sw.Stop();
            ts_1_50 = sw.Elapsed;
            Show(arr_1_50);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_2_50);
            sw.Restart();
            SortInsert(arr_2_50);
            sw.Stop();
            ts_2_50 = sw.Elapsed;
            Show(arr_2_50);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_2_50);
            sw.Restart();
            SortQuick(arr_3_50, 0, arr_3_50.Length - 1);
            sw.Stop();
            ts_3_50 = sw.Elapsed;
            Show(arr_2_50);
            Console.WriteLine("------------------------------------------------------");

            //сортируем 100
            Show(arr_1_100);
            sw.Restart();
            Array.Sort(arr_1_100);
            sw.Stop();
            ts_1_100 = sw.Elapsed;
            Show(arr_1_100);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_2_100);
            sw.Restart();
            SortInsert(arr_2_100);
            sw.Stop();
            ts_2_100 = sw.Elapsed;
            Show(arr_2_100);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_3_100);
            sw.Restart();
            SortQuick(arr_3_100, 0, arr_3_100.Length - 1);
            sw.Stop();
            ts_3_100 = sw.Elapsed;
            Show(arr_3_100);
            Console.WriteLine("------------------------------------------------------");

            //сортируем 200
            Show(arr_1_200);
            sw.Restart();
            Array.Sort(arr_1_200);
            sw.Stop();
            ts_1_200 = sw.Elapsed;
            Show(arr_1_200);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_2_200);
            sw.Restart();
            SortInsert(arr_2_200);
            sw.Stop();
            ts_2_200 = sw.Elapsed;
            Show(arr_2_200);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_3_200);
            sw.Restart();
            SortQuick(arr_3_200, 0, arr_3_200.Length - 1);
            sw.Stop();
            ts_3_200 = sw.Elapsed;
            Show(arr_3_200);
            Console.WriteLine("------------------------------------------------------");

            //сортируем 1000
            Show(arr_1_1000);
            sw.Restart();
            Array.Sort(arr_1_1000);
            sw.Stop();
            ts_1_1000 = sw.Elapsed;
            Show(arr_1_1000);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_2_1000);
            sw.Restart();
            SortInsert(arr_2_1000);
            sw.Stop();
            ts_2_1000 = sw.Elapsed;
            Show(arr_2_1000);
            Console.WriteLine("------------------------------------------------------");

            Show(arr_3_1000);
            sw.Restart();
            SortQuick(arr_3_1000, 0, arr_3_1000.Length - 1);
            sw.Stop();
            ts_3_1000 = sw.Elapsed;
            Show(arr_3_1000);
            Console.WriteLine("------------------------------------------------------");


            //-----------------------------------------
            //-----------таблица отчета----------------
            Console.WriteLine("для вывода таблици результатов замеров нажмите любую клавишу");
            Console.ReadKey();
            CTerminal terminal = new CTerminal(80, 20);
            CreateTable(terminal);
            terminal.PrintAt(14, 10, ts_1_50.TotalMilliseconds.ToString());
            terminal.PrintAt(30, 10, ts_1_100.TotalMilliseconds.ToString());
            terminal.PrintAt(46, 10, ts_1_200.TotalMilliseconds.ToString());
            terminal.PrintAt(62, 10, ts_1_1000.TotalMilliseconds.ToString());

            terminal.PrintAt(14, 6, ts_2_50.TotalMilliseconds.ToString());
            terminal.PrintAt(30, 6, ts_2_100.TotalMilliseconds.ToString());
            terminal.PrintAt(46, 6, ts_2_200.TotalMilliseconds.ToString());
            terminal.PrintAt(62, 6, ts_2_1000.TotalMilliseconds.ToString());

            terminal.PrintAt(14, 8, ts_3_50.TotalMilliseconds.ToString());
            terminal.PrintAt(30, 8, ts_3_100.TotalMilliseconds.ToString());
            terminal.PrintAt(46, 8, ts_3_200.TotalMilliseconds.ToString());
            terminal.PrintAt(62, 8, ts_3_1000.TotalMilliseconds.ToString());

            //-----------------------------------------
            Console.ReadKey();
            terminal.Close();
        }
        private static void Show(int[] arr)
        {
            foreach(int i in arr)
            {
                Console.Write($"{i} ");
            }
            Console.WriteLine();
        }

        private static void Generic(int[] arr, Random rnd)
        {
            for(int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(1, 600);
            }
        }

        private static void SortInsert(int[] arr)
        {
            int key, j;
            for(int i = 1; i < arr.Length; i++)
            {
                key = arr[i];
                j = i;
                while(j > 0 && arr[j - 1] > key)
                {
                    arr[j] = arr[j - 1];
                    j = j - 1;
                }
                arr[j] = key;
            }
        }
        private  static void SortQuick(int[] arr, long first, long last)
        {
            int p = arr[(last - first) / 2 + first];
            int tmp;
            long i = first, j = last;
            while(i <= j)
            {
                while (arr[i] < p && i <= last) ++i;
                while (arr[j] > p && j >= first) --j;
                if(i <= j)
                {
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                    ++i;
                    --j;
                }
            }
            if (j > first) SortQuick(arr, first, j);
            if (i < last) SortQuick(arr, i, last);
        }
        private static void CreateTable(CTerminal terminal)
        {
            string line_0 = $"{(char)9556}{new string((char)9552, 12)}{(char)9572}{new string((char)9552, 63)}{(char)9559}";
            string line_1 = $"{(char)9553}метод       {(char)9474}размер массива{new string(' ', 49)}{(char)9553}";
            string line_lin = new string((char)9472, 15);
            string line_lin_1 = $"{(char)9500}{line_lin}{(char)9516}{line_lin}{(char)9516}{line_lin}{(char)9516}{line_lin}{(char)9570}";
            string line_2 = $"{(char)9553}сортирования{line_lin_1}";
            string line_3 = $"{(char)9553}{new string(' ', 12)}{(char)9474}{new string(' ', 15)}{(char)9474}{new string(' ', 15)}{(char)9474}{new string(' ', 15)}{(char)9474}{new string(' ', 15)}{(char)9553}";
            string line_4 = $"{(char)9567}{new string((char)9472, 12)}{(char)9532}{line_lin}{(char)9532}{line_lin}{(char)9532}{line_lin}{(char)9532}{line_lin}{(char)9570}";
            string line_lin_2 = new string((char)9552, 15);
            string line_5 = $"{(char)9562}{new string((char)9552, 12)}{(char)9575}{line_lin_2}{(char)9575}{line_lin_2}{(char)9575}{line_lin_2}{(char)9575}{line_lin_2}{(char)9565}";

            terminal.PrintAt(0, 1, line_0);
            terminal.PrintAt(0, 2, line_1);
            terminal.PrintAt(0, 3, line_2);
            terminal.PrintAt(0, 4, line_3);
            terminal.PrintAt(0, 5, line_4);
            terminal.PrintAt(0, 6, line_3);
            terminal.PrintAt(0, 7, line_4);
            terminal.PrintAt(0, 8, line_3);
            terminal.PrintAt(0, 9, line_4);
            terminal.PrintAt(0, 10, line_3);
            terminal.PrintAt(0, 11, line_5);

            terminal.PrintAt(14, 4, "n=50");
            terminal.PrintAt(30, 4, "n=100");
            terminal.PrintAt(46, 4, "n=200");
            terminal.PrintAt(62, 4, "n=1000");

            terminal.PrintAt(1, 6, "вставками");
            terminal.PrintAt(1, 8, "быстрая");
            terminal.PrintAt(1, 10, "стандартный");
        }
    }
}
