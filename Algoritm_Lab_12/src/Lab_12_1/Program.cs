﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_12_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.Title = "Лабораторна робота №12.1";
            CBinaryTree<string> root =
                new CBinaryTree<string>("Товари",
                    new CBinaryTree<string>("Комп'ютерна техніка",
                        new CBinaryTree<string>("ПК",
                            new CBinaryTree<string>("модель пк 1"),
                            new CBinaryTree<string>("модель пк 2")),
                        new CBinaryTree<string>("Ноутбуки",
                            new CBinaryTree<string>("модель ноутбук 1"),
                            new CBinaryTree<string>("модель ноутбук 2"))),
                    new CBinaryTree<string>("Принтери",
                        new CBinaryTree<string>("Лазерні",
                            new CBinaryTree<string>("модель лазерний 1"),
                            new CBinaryTree<string>("модель лазерний 2")),
                        new CBinaryTree<string>("Струменеві",
                            new CBinaryTree<string>("модель струменева 1"),
                            new CBinaryTree<string>("модель струменева 2"))));
            root.GetStringList();

            Console.ReadKey();
        }
    }
}
