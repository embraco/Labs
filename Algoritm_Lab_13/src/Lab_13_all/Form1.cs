﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySDK;

namespace Lab_13_all
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            fm_l_error.Text = "";
            fm_l_error_2.Text = "";
            fm_l_error_3.Text = "";
            fm_tb_change_where.Text = "Регулярний";// "Приклад";
            fm_tb_change_hawe.Text = "Не регулярний";// "Лістинг";
            fm_tb_find.Text = "Регулярні вирази";
            Load += EventLoad;
            fm_b_check_1.Click += EventButtonCheck_1_Click;
            fm_b_check_2.Click += EventButtonCheck_2_Click;
            fm_b_find_1.Click += EventButtonFind_1_Click;
            fm_b_exchenge.Click += EventChangeClick;
            fm_b_clear.Click += EventClearClick;
            fm_b_clear_html.Click += EventClearHTMLClick;
            fm_b_parse.Click += EventParseClick;
        }

        private void EventParseClick(object sender, EventArgs e)
        {
            string msg = fm_tb_html.Text;
            fm_tb_html.Text = "";
            string[] new_msg = msg.Split('\n');
            foreach(string s in new_msg)
            {
                msg = Regex.Replace(s, @"<[^>]*>", ""); //удаление тегов
                msg = Regex.Replace(msg, @"^[\s]+", ""); //удаление пробелов из начала строки
                msg = Regex.Replace(msg, @"[\n]+", "\n"); //замена нескольких переносов на один
                msg = TrimNewLines(msg); //удаляем новую линию с начала и конца строки
                if (!string.IsNullOrEmpty(msg))
                    fm_tb_html.Text += $"{msg}\n";
            }
        }
        private string TrimNewLines(string str)
        {
            int _start = 0;
            while (_start < str.Length && str[_start] == '\n')
                _start++;
            int _end = str.Length - 1;
            while (_end >= 0 && str[_end] == '\n')
                _end--;
            if (_start > _end)
                return string.Empty;
            return str.Substring(_start, _end - _start + 1);
        }

        private void EventClearHTMLClick(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader("html.html");
            fm_tb_html.Text = sr.ReadToEnd();
        }

        private void EventClearClick(object s, EventArgs e)
        {
            StreamReader sr = new StreamReader("text.txt");
            fm_tb_text.Text = sr.ReadToEnd();
        }

        /// <summary>
        /// 4. Для фрагмента тексту з завдання 3 замініть фрагмент Приклад на Лістинг
        /// </summary>
        private void EventChangeClick(object s, EventArgs e)
        {
            Regex rg = new Regex(fm_tb_change_where.Text);
            fm_tb_text.Text = rg.Replace(fm_tb_text.Text, fm_tb_change_hawe.Text);
        }

        private void EventButtonFind_1_Click(object s, EventArgs e)
        {
            Regex rg;
            string msg = "Вираз не знайдено";
            rg = new Regex($"{fm_tb_find.Text}");
            if (rg.IsMatch(fm_tb_text.Text))
                msg = "Вираз знайдено";
            fm_l_error_3.Text = msg;
        }

        /// <summary>
        /// 2. Створіть програму, яка перевіряє правильнісь числового виразу (всі цифри).
        /// </summary>
        private void EventButtonCheck_2_Click(object s, EventArgs e)
        {
            Regex rg;
            string msg = "Помилка";
            rg = new Regex(@"^([0-9]{1,}$)"); //for number
            if (rg.IsMatch(fm_tb_number.Text))
                msg = "Вірно";
            fm_l_error_2.Text = msg;
        }

        /// <summary>
        /// 1. Створіть форму реєстрації з полями: Прізвище, Ім’я, поштова адреса,
        /// номер телефону.Перевірте правильність введення з використанням
        /// регулярних виразів.
        /// </summary>
        private void EventButtonCheck_1_Click(object s, EventArgs e)
        {
            Regex rg;
            string msg = "";
            rg = new Regex(@"^[А-ЯЁ][а-яё]+"); //for name
            if (!rg.IsMatch(fm_tb_name.Text))
                msg = $"{msg}:Ім'я";
            rg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                           @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                           @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"); //for email
            if (!rg.IsMatch(fm_tb_email.Text))
                msg = $"{msg}:email";
            rg = new Regex(@"\(?([0-9]{3})\)?[ ]?([0-9]{3})[ ]?([0-9]{4})"); //for phone
            if (!rg.IsMatch(fm_tb_phone.Text))
                msg = $"{msg}:Номер";
            if (msg.Length > 0)
                msg = $"Помилка у{msg}";
            else
                msg = "Помилок немає";
            fm_l_error.Text = msg;
        }

        private void EventLoad(object s, EventArgs e)
        {
            CRM rm = new CRM();
            fm_tss_owner.Text = rm.GetRecource("owner");
            StreamReader sr = new StreamReader("text.txt");
            fm_tb_text.Text = sr.ReadToEnd();
            sr = new StreamReader("html.html");
            fm_tb_html.Text = sr.ReadToEnd();
        }
    }
}
