﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Lab_8_1
{
    //прототип консоли
    class CTerminal
    {
        private int saveBufferWidth;
        private int saveBufferHeight;
        private int saveWindowHeight;
        private int saveWindowWidth;
        private bool saveCursorVisible;
        private ConsoleColor saveConsoleColor;

        private int m_Width;
        private int m_Height;
        private char[][] m_Buffer;

        public CTerminal(int width, int height)
        {
            m_Width = width;
            m_Height = height;

            //сохранение стандарта
            saveBufferWidth = Console.BufferWidth;
            saveBufferHeight = Console.BufferHeight;
            saveWindowHeight = Console.WindowHeight;
            saveWindowWidth = Console.WindowWidth;
            saveCursorVisible = Console.CursorVisible;
            saveConsoleColor = Console.ForegroundColor;
            Console.Clear();
/*
    |
    |Height
    |
    +--Width-->
*/
            //установка окна
            Console.SetWindowSize(1, 1);
            Console.SetBufferSize(m_Width, m_Height);
            Console.SetWindowSize(m_Width, m_Height);

            //--------------------------------------------
            ResourceManager rm = new ResourceManager("Lab_8_1.db", Assembly.GetExecutingAssembly());
            char[] msg = rm.GetString("name").ToCharArray();
            for (int i = 0; i < msg.Length; i++) msg[i] = (char)(msg[i] - 2);

            Console.Title = rm.GetString("title");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.CursorVisible = false;
            m_Buffer = new char[m_Height][];
            for (int i = 0; i < m_Height; i++)
            {
                m_Buffer[i] = new char[m_Width];
            }
            PrintAt(0, m_Height - 1, new string(msg));
        }

        public void Close()
        {
            //возврат на стандарт
            //Console.CursorVisible = false;
            Console.Clear();
            Console.SetWindowPosition(0, 0);

            Console.Clear();
            Console.SetWindowSize(1, 1);
            Console.SetBufferSize(saveBufferWidth, saveBufferHeight);
            Console.SetWindowSize(saveWindowWidth, saveWindowHeight);
            Console.ForegroundColor = saveConsoleColor;
            Console.CursorVisible = saveCursorVisible;
        }

        public void PrintAt(int _x, int _y, string _msg)
        {
            int i = _x;
            foreach(char c in _msg)
            {
                if(i < m_Width)
                {
                    m_Buffer[_y][i] = c;
                    i++;
                }
            }
            Console.Clear();
            foreach (char[] c in m_Buffer)
            {
                Console.Write(c);
            }
        }
    }
}
