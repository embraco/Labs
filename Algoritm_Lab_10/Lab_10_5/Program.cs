﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySDK;

namespace Lab_10_5
{
    /// <summary>
    /// 5. Методи роботи з множинами Linq
    /// Створити 2 колекції List<int> з 20 випадкових цілих чисел
    /// в діапазоні від 10 до 50. 
    /// Виконати об’єднання, перетин та різницю цих колекцій.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(120, 24);
            _terminal.Title = "Лабораторна робота №10.5";

            List<int> _list_one = new List<int>();
            List<int> _list_thwo = new List<int>();
            Random _rnd = new Random();

            for(int i = 0; i < 20; i++)
            {
                _list_one.Add(_rnd.Next(10, 50));
                _list_thwo.Add(_rnd.Next(10, 50));
            }

            _terminal.PrintAt(0, 0, "Перша множина");
            _terminal.PrintAt(0, 1, GetArrString<int>(_list_one));
            _terminal.PrintAt(0, 2, "Друга множина");
            _terminal.PrintAt(0, 3, GetArrString<int>(_list_thwo));
            var _list_tmp = _list_one.Union(_list_thwo);
            _terminal.PrintAt(0, 4, "Об'єднання множин");
            _terminal.PrintAt(0, 5, GetArrString<int>(_list_tmp));
            _list_tmp = _list_one.Intersect(_list_thwo);
            _terminal.PrintAt(0, 6, "Перетин множин");
            _terminal.PrintAt(0, 7, GetArrString<int>(_list_tmp));
            _list_tmp = _list_one.Except(_list_thwo);
            _terminal.PrintAt(0, 8, "Рiзниця множин");
            _terminal.PrintAt(0, 9, GetArrString<int>(_list_tmp));


            Console.ReadKey();
            _terminal.Close();
        }

        private static string GetArrString<T>(List<T> _arr)
        {
            string _msg = "{";
            bool _first = true;
            foreach (T _elem in _arr)
            {
                if (_first)
                {
                    _first = !_first;
                    _msg += $"{_elem}";
                }
                else
                {
                    _msg += $",{_elem}";
                }
            }
            _msg += "}";
            return _msg;
        }

        private static string GetArrString<T>(IEnumerable<T> _arr)
        {
            string _msg = "{";
            bool _first = true;
            foreach (T _elem in _arr)
            {
                if (_first)
                {
                    _first = !_first;
                    _msg += $"{_elem}";
                }
                else
                {
                    _msg += $",{_elem}";
                }
            }
            _msg += "}";
            return _msg;
        }
    }
}
