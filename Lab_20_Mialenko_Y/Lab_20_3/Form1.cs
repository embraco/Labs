﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_20_3
{
    /// <summary>
    /// 3. Створити Windows-проект форми Анкета студента. На формі
    /// вказати назву Лаб. 20, розмістити підписи(Label), текстові поля
    /// (TexBox) і кнопки(Button) OK i Close.Властивостям кнопок
    /// призначити можливість реагування на клавіатуру.Вказати при
    /// реєстрації такі дані: Ім’я, Прізвище, Рік народження, Група, Курс.
    /// Ввести дані в текстові поля.При натисненні на кнопку OK
    /// прочитати дані з перевіркою допустимості, і вивести їх в додаткове
    /// текстове багаторядкове поле.
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Load += LoadForm;
            btn_close.Click += CloseForm;
            btn_ok.Click += Calculate;

            input_f_name.KeyPress += StringKey;
            input_s_name.KeyPress += StringKey;
            input_b_date.KeyPress += DigitKey;
            input_cours.KeyPress += DigitKey;
        }

        //мой код
        private void LoadForm(object s, EventArgs e)
        {
            ResourceManager rm = new ResourceManager("Lab_20_3.db", Assembly.GetExecutingAssembly());
            char[] msg = rm.GetString("name").ToCharArray();
            for (int i = 0; i < msg.Length; i++) msg[i] = (char)(msg[i] - 2);
            this.toolStripStatusLabel2.Text = new string(msg);
            this.Text += " №20.3";
            f_name.Text = rm.GetString("f_name");
            s_name.Text = rm.GetString("s_name");
            b_date.Text = rm.GetString("b_date");
            group.Text  = rm.GetString("group");
            cours.Text  = rm.GetString("cours");
        }

        private void CloseForm(object s, EventArgs e)
        {
            Close();
        }


        private void StringKey(object s, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Поле тільки для літер");
            }
        }
        private void DigitKey(object s, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Поле містить тільки цифри");
            }
        }
        private void Calculate(object s, EventArgs e)
        {
            string f_n = input_f_name.Text;;
            string s_n = input_s_name.Text;
            string b_d = input_b_date.Text;
            string gru = input_group.Text;
            string cur = input_cours.Text;
            string nl = Environment.NewLine;
            string msg = $"{f_n}{nl}{s_n}{nl}{b_d}{nl}{gru}{nl}{cur}";

            text_res.Text = msg;
        }
    }
}
