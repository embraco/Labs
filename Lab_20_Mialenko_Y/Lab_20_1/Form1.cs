﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_20_1
{
    /// <summary>
    /// 1. Написати програму розрахунку вартості бензину на заправці.
    /// Вхідні дані – кількість літрів, марка бензину(92,95,98) і наявність
    /// знижки(дисконтної картки).
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Load += LoadForm;
            btn_exit.Click += CloseForm;
            btn_calculate.Click += Calculate;
            intput_count.TextChanged += Vallidate;
        }

        //мой код
        private void LoadForm(object s, EventArgs e)
        {
            ResourceManager rm = new ResourceManager("Lab_20_1.db", Assembly.GetExecutingAssembly());
            char[] msg = rm.GetString("name").ToCharArray();
            for (int i = 0; i < msg.Length; i++) msg[i] = (char)(msg[i] - 2);
            this.toolStripStatusLabel2.Text = new string(msg);
            this.label_marks.Text = rm.GetString("label_marks");
            this.label_input_count.Text = rm.GetString("lable_input_count");
            this.Text += " №20.1";
        }

        private void CloseForm(object s, EventArgs e)
        {
            Close();
        }

        private void Calculate(object s, EventArgs e)
        {
            if(chek_a92.Checked || chek_a95.Checked || chek_a98.Checked)
            {
                
                int count;
                if (int.TryParse(intput_count.Text, out count))
                {
                    double res = 
                        chek_a92.Checked ? count * 15 :
                        chek_a95.Checked ? count * 25 :
                        chek_a98.Checked ? count * 35 : 0;
                    res = cBox_discount.Checked ? (res * 0.50) : res;
                    MessageBox.Show($"Ціна: {res}");
                }
                else
                {
                    MessageBox.Show("Треба ввести ціле число");
                }
            }
            else
            {
                MessageBox.Show("Оберіть марку палива");
            }
        }

        private void Vallidate(object s, EventArgs e)
        {
            if(intput_count.Text.Length > 0)
            {
                foreach (char c in intput_count.Text)
                {
                    if (!char.IsDigit(c))
                        MessageBox.Show("Потрібно ввести ціле або дійсне число");
                }
            }
            
        }
    }
}
