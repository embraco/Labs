﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_12_2
{
    class CBinaryTreeNode<T> : IComparable<CBinaryTreeNode<T>> where T : IComparable<T>
    {
        internal T value;
        internal CBinaryTreeNode<T> parent;
        internal CBinaryTreeNode<T> leftChild;
        internal CBinaryTreeNode<T> rightChild;

        public CBinaryTreeNode(T _value)
        {
            if (value == null)
                throw new ArgumentNullException("Не может быть null");
            value = _value;
            parent = null;
            leftChild = null;
            rightChild = null;
        }

        public override string ToString()
        {
            return value.ToString();
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            CBinaryTreeNode<T> other = (CBinaryTreeNode<T>)obj;
            return CompareTo(other) == 0;
        }

        public int CompareTo(CBinaryTreeNode<T> other)
        {
            return value.CompareTo(other.value);
        }
    }
}
