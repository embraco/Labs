﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySDK;

namespace Lab_10_3
{
    /// <summary>
    /// 3. Клас SortedSet<T>
    /// Створити множину з 10 додатних чисел і 0.
    /// Створити множину з 10 від’ємних чисел і 0.
    /// Створити об’єднання та перетин цих множин.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CTerminal _terminal = new CTerminal(120, 24);
            _terminal.Title = "Лабораторна робота №10.3";

            SortedSet<int> _mass_one = new SortedSet<int>() { 9, 8, 7, 6, 0, 2, 3, 4, 1, 5 };
            SortedSet<int> _mass_thwo = new SortedSet<int>() { -6, -7, -8, -9, -0, -1, -3, -4, -2, -5 };

            _terminal.PrintAt(0, 0, "Перша множина");
            _terminal.PrintAt(0, 1, GetHashString<int>(_mass_one));
            _terminal.PrintAt(0, 2, "Друга множина");
            _terminal.PrintAt(0, 3, GetHashString<int>(_mass_thwo));
            _terminal.PrintAt(0, 4, "Об'єднання множин");
            SortedSet<int> _mass_tmp = new SortedSet<int>(_mass_one);
            _mass_tmp.UnionWith(_mass_thwo);
            _terminal.PrintAt(0, 5, GetHashString<int>(_mass_tmp));
            _terminal.PrintAt(0, 6, "Перетин множин");
            _mass_tmp = new SortedSet<int>(_mass_one);
            _mass_tmp.IntersectWith(_mass_thwo);
            _terminal.PrintAt(0, 7, GetHashString<int>(_mass_tmp));

            Console.ReadKey();
            _terminal.Close();
        }

        private static string GetHashString<T>(SortedSet<T> _value)
        {
            string _msg = "{";
            bool _first = true;
            foreach (T _elem in _value)
            {
                if (_first)
                {
                    _first = !_first;
                    _msg += $"{_elem}";
                }
                else
                {
                    _msg += $",{_elem}";
                }
            }
            _msg += "}";
            return _msg;
        }
    }
}
