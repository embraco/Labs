﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab_23_Forms;

namespace Lab_23_3
{
    static class Program
    {
        private static FLab_23_3 m_form;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            m_form = new FLab_23_3();
            m_form.BExit.Click += (s, e) =>
            {
                Application.Exit();
            };
            m_form.lbList.Items.AddRange(new object[] {"index0",
                                                       "index1",
                                                       "index2",
                                                       "index3",
                                                       "index4"
                                                        });
            m_form.lbList.SelectedIndexChanged += (s, e) =>
            {
                m_form.pbImage.Image = (Image)m_form.GetResource.GetObject(m_form.lbList.SelectedItem.ToString());
            };
            Application.Run(m_form);
        }
    }
}
