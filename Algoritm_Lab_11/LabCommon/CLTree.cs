﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabCommon
{
    public class CLTree<T>
    {
        private T m_data;
        private CLTree<T> m_partent;
        private ICollection<CLTree<T>> m_children;

        //>>>интерфейс<<<
        public CLTree(T _value)
        {
            m_partent = null;
            m_data = _value;
            m_children = null;
        }

        public CLTree<T> AddChild(T _value)
        {
            if(m_children == null)
                m_children = new LinkedList<CLTree<T>>();
            CLTree<T> _child = new CLTree<T>(_value);
            _child.Parent = this;
            m_children.Add(_child);
            return _child;
        }

        public T Data => m_data;
        public CLTree<T> Parent
        {
            get { return m_partent; }
            set { m_partent = value; }
        }
        public ICollection<CLTree<T>> Child => m_children;

        public List<string> GetListStringTree()
        {
            List<string> _list = new List<string>();
            BuildListStringTree(this , 0, _list);
            return _list;
        }
        private void BuildListStringTree(CLTree<T> _node, int _depth, List<string> _list)
        {
            if (_node == null)
                return;
            _list.Add($"{new string(' ',_depth * 2)}{_depth}:{_node.Data}");
            if (_node.Child == null)
                return;
            foreach(CLTree<T> el in _node.Child)
            {
                BuildListStringTree(el, _depth + 1, _list);
            }
        }
    }
}
