﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabCommon
{
    public class CTree<T>
    {
        //поля класса
        private CTreeNone<T> m_root;
        List<string> m_stringTree;
        //конструкторы
        public CTree(T _value)
        {
            if (_value == null)
                throw new ArgumentNullException("Не может содержать null");
            m_root = new CTreeNone<T>(_value);
            m_stringTree = new List<string>();
        }

        public CTree(T _value, params CTree<T>[] _child)
            : this(_value)
        {
            foreach(CTree<T> _elem in _child)
            {
                m_root.AddChild(_elem.m_root);
            }
        }

        // >>>интерфейс<<<
        public CTreeNone<T> Root => m_root;

        public List<string> GetStringTree()
        {
            m_stringTree.Clear();
            BuildStringTree(m_root, 0);
            return m_stringTree;
        }

        //для взаимодействия с MySDK переписал метод
        private void BuildStringTree(CTreeNone<T> _root, int _dept)
        {
            if (m_root == null)
                return;
            m_stringTree.Add($"{new string(' ', _dept * 2)}{_dept}:{_root.Value}");
            CTreeNone<T> _child = null;
            for(int i = 0; i < _root.ChildrenCount; i++)
            {
                _child = _root[i];
                BuildStringTree(_child, _dept + 1);
            }
        }
    }
}
