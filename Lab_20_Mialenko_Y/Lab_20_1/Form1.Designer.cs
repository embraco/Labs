﻿
using System.Reflection;
using System.Resources;

namespace Lab_20_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_input_count = new System.Windows.Forms.Label();
            this.intput_count = new System.Windows.Forms.TextBox();
            this.chek_a92 = new System.Windows.Forms.RadioButton();
            this.chek_a95 = new System.Windows.Forms.RadioButton();
            this.chek_a98 = new System.Windows.Forms.RadioButton();
            this.label_marks = new System.Windows.Forms.Label();
            this.cBox_discount = new System.Windows.Forms.CheckBox();
            this.btn_calculate = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_input_count
            // 
            this.label_input_count.AutoSize = true;
            this.label_input_count.Location = new System.Drawing.Point(12, 68);
            this.label_input_count.Name = "label_input_count";
            this.label_input_count.Size = new System.Drawing.Size(39, 13);
            this.label_input_count.TabIndex = 0;
            this.label_input_count.Text = "Lable2";
            // 
            // intput_count
            // 
            this.intput_count.Location = new System.Drawing.Point(101, 65);
            this.intput_count.Name = "intput_count";
            this.intput_count.Size = new System.Drawing.Size(88, 20);
            this.intput_count.TabIndex = 1;
            // 
            // chek_a92
            // 
            this.chek_a92.AutoSize = true;
            this.chek_a92.Location = new System.Drawing.Point(15, 25);
            this.chek_a92.Name = "chek_a92";
            this.chek_a92.Size = new System.Drawing.Size(44, 17);
            this.chek_a92.TabIndex = 2;
            this.chek_a92.TabStop = true;
            this.chek_a92.Text = "A92";
            this.chek_a92.UseVisualStyleBackColor = true;
            // 
            // chek_a95
            // 
            this.chek_a95.AutoSize = true;
            this.chek_a95.Location = new System.Drawing.Point(65, 25);
            this.chek_a95.Name = "chek_a95";
            this.chek_a95.Size = new System.Drawing.Size(44, 17);
            this.chek_a95.TabIndex = 3;
            this.chek_a95.TabStop = true;
            this.chek_a95.Text = "A95";
            this.chek_a95.UseVisualStyleBackColor = true;
            // 
            // chek_a98
            // 
            this.chek_a98.AutoSize = true;
            this.chek_a98.Location = new System.Drawing.Point(115, 25);
            this.chek_a98.Name = "chek_a98";
            this.chek_a98.Size = new System.Drawing.Size(44, 17);
            this.chek_a98.TabIndex = 4;
            this.chek_a98.TabStop = true;
            this.chek_a98.Text = "A98";
            this.chek_a98.UseVisualStyleBackColor = true;
            // 
            // label_marks
            // 
            this.label_marks.AutoSize = true;
            this.label_marks.Location = new System.Drawing.Point(12, 9);
            this.label_marks.Name = "label_marks";
            this.label_marks.Size = new System.Drawing.Size(39, 13);
            this.label_marks.TabIndex = 5;
            this.label_marks.Text = "Lable1";
            // 
            // cBox_discount
            // 
            this.cBox_discount.AutoSize = true;
            this.cBox_discount.Location = new System.Drawing.Point(15, 48);
            this.cBox_discount.Name = "cBox_discount";
            this.cBox_discount.Size = new System.Drawing.Size(118, 17);
            this.cBox_discount.TabIndex = 6;
            this.cBox_discount.Text = "Наявність знижки";
            this.cBox_discount.UseVisualStyleBackColor = true;
            // 
            // btn_calculate
            // 
            this.btn_calculate.Location = new System.Drawing.Point(12, 91);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(116, 23);
            this.btn_calculate.TabIndex = 8;
            this.btn_calculate.Text = "Розрахувати";
            this.btn_calculate.UseVisualStyleBackColor = true;
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(148, 91);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 9;
            this.btn_exit.Text = "Вийти";
            this.btn_exit.UseVisualStyleBackColor = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(53, 17);
            this.toolStripStatusLabel1.Text = "Виконав";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(28, 17);
            this.toolStripStatusLabel2.Text = "Ім\'я";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 118);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(335, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 140);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_calculate);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.cBox_discount);
            this.Controls.Add(this.label_marks);
            this.Controls.Add(this.chek_a98);
            this.Controls.Add(this.chek_a95);
            this.Controls.Add(this.chek_a92);
            this.Controls.Add(this.intput_count);
            this.Controls.Add(this.label_input_count);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Лабораторна робота";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_input_count;
        private System.Windows.Forms.TextBox intput_count;
        private System.Windows.Forms.RadioButton chek_a92;
        private System.Windows.Forms.RadioButton chek_a95;
        private System.Windows.Forms.RadioButton chek_a98;
        private System.Windows.Forms.Label label_marks;
        private System.Windows.Forms.CheckBox cBox_discount;
        private System.Windows.Forms.Button btn_calculate;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
    }
}

