﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lab_22_Forms;

namespace Lab_22_2
{
    /// <summary>
    /// 3. Написати програму розрахунку вартості замовлення в McDonalds
    /// з використанням CheckedListBox з можливістю додавання.
    /// </summary>


    struct SItem
    {
        public string   _name;
        public int      _count;
        public double   _price;
        public SItem(string _n, int _c, double _p)
        {
            _name = _n;
            _count = _c;
            _price = _p;
        }
        public override string ToString()
        {
            //string.Format("{0} кіл:{1} ціна:{2}", _name, _count, _price);
            return $"{_name} кіл:{_count} ціна:{_price}";
        }
    }

    static class Program
    {
        private static FLab_22_2 mf_form;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //создание и бинд инициализации на форму---------------
            mf_form = new FLab_22_2();
            mf_form.Load += EventFLoad;
            //-----------------------------------------------------

            Application.Run(mf_form);
        }

        private static void EventFLoad(object s, EventArgs e)
        {
            //
            mf_form.bExit.Click += EventExit;
            mf_form.clbMenu.Items.AddRange(new object[]{
                                                new SItem("Гамбургер", 1, 23.3),
                                                new SItem("Картошка фри малая", 1, 22.2),
                                                new SItem("Колла", 1, 21.1)
                                            });

            mf_form.bAddToMenu.Enabled              = false;
            mf_form.bOrder.Enabled                  = false;
            mf_form.nCount.Enabled                  = false;
            mf_form.tbPrice.Enabled                 = false;
            mf_form.tbInput.TextChanged             += EventInitialAddMenu;
            mf_form.clbMenu.SelectedIndexChanged    += EventElemMenuChange;
            mf_form.bAddToMenu.Click                += EventAddToMenu;
            mf_form.bOrder.Click                    += EventOrder;
            mf_form.tbPrice.TextChanged             += EventKeyPresDigit;
            mf_form.tbPrice.Text = "0";
            //-----------------------------------------------------
        }

        private static void EventKeyPresDigit(object s, EventArgs e)
        {
            double _price;
            mf_form.tbPrice.Text = mf_form.tbPrice.Text.Length == 0 ? "0" : mf_form.tbPrice.Text;
            if (!double.TryParse(mf_form.tbPrice.Text, out _price))
            {
                mf_form.bAddToMenu.Enabled = false;
                MessageBox.Show("Можна ввести ціле або дійсне число");
            }
            else
                mf_form.bAddToMenu.Enabled = true;
        }

        private static void EventOrder(object s, EventArgs e)
        {
            mf_form.tbOutput.Text = "";
            string _nl = Environment.NewLine;
            double _order = 0.0;
            for(int i = 0; i < mf_form.clbMenu.CheckedItems.Count; i++)
            {
                SItem _test = (SItem)mf_form.clbMenu.CheckedItems[i];
                string _name = mf_form.clbMenu.CheckedItems[i].ToString();
                double _count = (double)_test._count * _test._price;
                _order += _count;
                mf_form.tbOutput.Text += $"{_name} | {_count}{_nl}";
            }
            mf_form.tbOutput.Text += $"Ціна за все: {_order}{_nl}";
            for (int i = 0; i < mf_form.clbMenu.Items.Count; i++)
            {
                mf_form.clbMenu.SetItemCheckState(i, CheckState.Unchecked);
            }
            mf_form.bOrder.Enabled = false;
        }

        private static void EventElemMenuChange(object s, EventArgs e)
        {
            //MessageBox.Show("что то изменилось в меню заказа");
            if (mf_form.clbMenu.CheckedItems.Count != 0)
                mf_form.bOrder.Enabled = true;
            else
                mf_form.bOrder.Enabled = false;
        }

        private static void EventAddToMenu(object s, EventArgs e)
        {
            double _price;
            double.TryParse(mf_form.tbPrice.Text, out _price);
            mf_form.clbMenu.Items.Add(new SItem(mf_form.tbInput.Text, (int)mf_form.nCount.Value, _price));
            mf_form.tbInput.Text = "";
        }

        private static void EventInitialAddMenu(object s, EventArgs e)
        {
            if (mf_form.tbInput.Text.Length > 0)
            {
                mf_form.bAddToMenu.Enabled = true;
                mf_form.nCount.Enabled = true;
                mf_form.tbPrice.Enabled = true;
            }
            else
            {
                mf_form.bAddToMenu.Enabled = false;
                mf_form.nCount.Enabled = false;
                mf_form.tbPrice.Enabled = false;
            }

        }

        private static void EventExit(object s, EventArgs e)
        {
            Application.Exit();
        }
    }
}
