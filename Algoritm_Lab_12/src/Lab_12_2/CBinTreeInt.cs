﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_12_2
{
    class CBinTreeInt : CBinaryTree<int>
    {
        public int GetSum()
        {
            int sum = 0;
            GetSumRecursion(root, ref sum);
            return sum;
        }

        private static void GetSumRecursion(CBinaryTreeNode<int> node, ref int sum)
        {
            sum += node.value;
            if (node.leftChild != null)
                GetSumRecursion(node.leftChild, ref sum);
            if (node.rightChild != null)
                GetSumRecursion(node.rightChild, ref sum);
        }
    }
}
