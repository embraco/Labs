﻿
namespace Lab_11_5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Департаменти");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("кафедра КНІС");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("кафедра математики");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Факультет КННІ", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("МЕ-81");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("кафедра міжнародної економіки", new System.Windows.Forms.TreeNode[] {
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("кафедра Облік і аудит");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("ФН-71");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Фінанси", new System.Windows.Forms.TreeNode[] {
            treeNode8});
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Факультет економіки і менеджменту", new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7,
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Факультет фізреабілітації");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("МНТУ", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode4,
            treeNode10,
            treeNode11});
            this.fm_tv_list = new System.Windows.Forms.TreeView();
            this.fm_lb_list = new System.Windows.Forms.ListBox();
            this.fm_tb_name = new System.Windows.Forms.TextBox();
            this.fm_tb_text = new System.Windows.Forms.TextBox();
            this.fm_b_add = new System.Windows.Forms.Button();
            this.fm_b_delete = new System.Windows.Forms.Button();
            this.fm_l_name = new System.Windows.Forms.Label();
            this.fm_l_text = new System.Windows.Forms.Label();
            this.fm_b_exit = new System.Windows.Forms.Button();
            this.fm_b_list = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.fm_tlss_ovner = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fm_tv_list
            // 
            this.fm_tv_list.Location = new System.Drawing.Point(12, 12);
            this.fm_tv_list.Name = "fm_tv_list";
            treeNode1.Name = "node_1_1";
            treeNode1.Text = "Департаменти";
            treeNode2.Name = "node_2_1";
            treeNode2.Text = "кафедра КНІС";
            treeNode3.Name = "node_2_2";
            treeNode3.Text = "кафедра математики";
            treeNode4.Name = "node_1_2";
            treeNode4.Text = "Факультет КННІ";
            treeNode5.Name = "node_3_1";
            treeNode5.Text = "МЕ-81";
            treeNode6.Name = "node_2_1";
            treeNode6.Text = "кафедра міжнародної економіки";
            treeNode7.Name = "node_2_2";
            treeNode7.Text = "кафедра Облік і аудит";
            treeNode8.Name = "node_3_1";
            treeNode8.Text = "ФН-71";
            treeNode9.Name = "node_2_3";
            treeNode9.Text = "Фінанси";
            treeNode10.Name = "node_1_3";
            treeNode10.Text = "Факультет економіки і менеджменту";
            treeNode11.Name = "node_1_4";
            treeNode11.Text = "Факультет фізреабілітації";
            treeNode12.Name = "node_0";
            treeNode12.Text = "МНТУ";
            this.fm_tv_list.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode12});
            this.fm_tv_list.Size = new System.Drawing.Size(261, 420);
            this.fm_tv_list.TabIndex = 0;
            // 
            // fm_lb_list
            // 
            this.fm_lb_list.FormattingEnabled = true;
            this.fm_lb_list.Location = new System.Drawing.Point(279, 12);
            this.fm_lb_list.Name = "fm_lb_list";
            this.fm_lb_list.Size = new System.Drawing.Size(361, 420);
            this.fm_lb_list.TabIndex = 2;
            // 
            // fm_tb_name
            // 
            this.fm_tb_name.Location = new System.Drawing.Point(646, 12);
            this.fm_tb_name.Name = "fm_tb_name";
            this.fm_tb_name.Size = new System.Drawing.Size(196, 20);
            this.fm_tb_name.TabIndex = 3;
            // 
            // fm_tb_text
            // 
            this.fm_tb_text.Location = new System.Drawing.Point(646, 39);
            this.fm_tb_text.Name = "fm_tb_text";
            this.fm_tb_text.Size = new System.Drawing.Size(196, 20);
            this.fm_tb_text.TabIndex = 4;
            // 
            // fm_b_add
            // 
            this.fm_b_add.Location = new System.Drawing.Point(646, 66);
            this.fm_b_add.Name = "fm_b_add";
            this.fm_b_add.Size = new System.Drawing.Size(75, 23);
            this.fm_b_add.TabIndex = 5;
            this.fm_b_add.Text = "Додати";
            this.fm_b_add.UseVisualStyleBackColor = true;
            // 
            // fm_b_delete
            // 
            this.fm_b_delete.Location = new System.Drawing.Point(646, 96);
            this.fm_b_delete.Name = "fm_b_delete";
            this.fm_b_delete.Size = new System.Drawing.Size(75, 23);
            this.fm_b_delete.TabIndex = 6;
            this.fm_b_delete.Text = "Видалити";
            this.fm_b_delete.UseVisualStyleBackColor = true;
            // 
            // fm_l_name
            // 
            this.fm_l_name.AutoSize = true;
            this.fm_l_name.Location = new System.Drawing.Point(849, 15);
            this.fm_l_name.Name = "fm_l_name";
            this.fm_l_name.Size = new System.Drawing.Size(35, 13);
            this.fm_l_name.TabIndex = 7;
            this.fm_l_name.Text = "Name";
            // 
            // fm_l_text
            // 
            this.fm_l_text.AutoSize = true;
            this.fm_l_text.Location = new System.Drawing.Point(849, 42);
            this.fm_l_text.Name = "fm_l_text";
            this.fm_l_text.Size = new System.Drawing.Size(28, 13);
            this.fm_l_text.TabIndex = 8;
            this.fm_l_text.Text = "Text";
            // 
            // fm_b_exit
            // 
            this.fm_b_exit.Location = new System.Drawing.Point(646, 126);
            this.fm_b_exit.Name = "fm_b_exit";
            this.fm_b_exit.Size = new System.Drawing.Size(75, 23);
            this.fm_b_exit.TabIndex = 9;
            this.fm_b_exit.Text = "Вийти";
            this.fm_b_exit.UseVisualStyleBackColor = true;
            // 
            // fm_b_list
            // 
            this.fm_b_list.Location = new System.Drawing.Point(728, 65);
            this.fm_b_list.Name = "fm_b_list";
            this.fm_b_list.Size = new System.Drawing.Size(75, 23);
            this.fm_b_list.TabIndex = 10;
            this.fm_b_list.Text = "Обхід";
            this.fm_b_list.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fm_tlss_ovner});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(894, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // fm_tlss_ovner
            // 
            this.fm_tlss_ovner.Name = "fm_tlss_ovner";
            this.fm_tlss_ovner.Size = new System.Drawing.Size(28, 17);
            this.fm_tlss_ovner.Text = "Ім\'я";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 461);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.fm_b_list);
            this.Controls.Add(this.fm_b_exit);
            this.Controls.Add(this.fm_l_text);
            this.Controls.Add(this.fm_l_name);
            this.Controls.Add(this.fm_b_delete);
            this.Controls.Add(this.fm_b_add);
            this.Controls.Add(this.fm_tb_text);
            this.Controls.Add(this.fm_tb_name);
            this.Controls.Add(this.fm_lb_list);
            this.Controls.Add(this.fm_tv_list);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView fm_tv_list;
        private System.Windows.Forms.ListBox fm_lb_list;
        private System.Windows.Forms.TextBox fm_tb_name;
        private System.Windows.Forms.TextBox fm_tb_text;
        private System.Windows.Forms.Button fm_b_add;
        private System.Windows.Forms.Button fm_b_delete;
        private System.Windows.Forms.Label fm_l_name;
        private System.Windows.Forms.Label fm_l_text;
        private System.Windows.Forms.Button fm_b_exit;
        private System.Windows.Forms.Button fm_b_list;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel fm_tlss_ovner;
    }
}

